//
//  BorderViewPopUp.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/6/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

extension UIView {
    func roundedCorners(top: Bool){
        let corners:UIRectCorner = (top ? [.topLeft , .topRight] : [.bottomRight , .bottomLeft])
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii:CGSize(width:15.0, height:15.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
    }
    
    
    func roundedCornersAll(){
        let corners:UIRectCorner = [.topLeft , .topRight, .bottomRight, .bottomLeft ]
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii:CGSize(width:15.0, height:15.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
    }
    
}
