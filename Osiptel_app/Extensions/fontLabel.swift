//
//  fontLabel.swift
//  Osiptel_app
//
//  Created by GYS on 4/22/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func regular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "MuseoSans", size: size)!
    }
}
