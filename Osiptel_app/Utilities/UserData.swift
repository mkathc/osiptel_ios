//
//  UserData.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/13/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//
import Foundation

extension UserDefaults{
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func setCalificanos(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isCalificanos.rawValue)
        //synchronize()
    }
    
    func isCalificanos()-> Bool {
        return bool(forKey: UserDefaultsKeys.isCalificanos.rawValue)
    }
    
    
    //MARK: Save User Data
    func setUserID(value: String){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getUserID() -> String{
        return string(forKey: UserDefaultsKeys.userID.rawValue) as? String ?? ""
    }
    
    //MARK: Save User Data
    func setName(value: String){
        set(value, forKey: UserDefaultsKeys.name.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getName() -> String{
        return string(forKey: UserDefaultsKeys.name.rawValue) as? String ?? ""
    }
    
    func setLastnameP(value: String){
        set(value, forKey: UserDefaultsKeys.lastnameP.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getLastnameP() -> String{
        return string(forKey: UserDefaultsKeys.lastnameP.rawValue) as? String ?? ""
    }
    
    //MARK: Retrieve User Data
    func getLastnameM() -> String{
        return string(forKey: UserDefaultsKeys.lastnameM.rawValue) as? String ?? ""
    }
    
    func setLastnameM(value: String){
        set(value, forKey: UserDefaultsKeys.lastnameM.rawValue)
        //synchronize()
    }
    
    func setNumber(value: String){
        set(value, forKey: UserDefaultsKeys.number.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getNumber() -> String{
        return string(forKey: UserDefaultsKeys.number.rawValue) as? String ?? ""
    }
    
    func setEmail(value: String){
        set(value, forKey: UserDefaultsKeys.email.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getEmail() -> String{
        return string(forKey: UserDefaultsKeys.email.rawValue) as? String ?? ""
    }
    
    func setNameComplete(value: String){
        set(value, forKey: UserDefaultsKeys.nameComplete.rawValue)
        //synchronize()
    }
    
    //MARK: Retrieve User Data
    func getNameComplete() -> String{
        return string(forKey: UserDefaultsKeys.nameComplete.rawValue) as? String ?? ""
    }

    //Dictionary
    func setOpciones(value: [[String:AnyObject]]){
        set(value, forKey: UserDefaultsKeys.opciones.rawValue)
    }
    
    func getOpciones() -> [[String:AnyObject]]{
        return array(forKey: UserDefaultsKeys.opciones.rawValue) as? [[String : AnyObject]] ?? [[:]]
    }
    
    //UUID
    func setUUID(value: String){
        set(value, forKey: UserDefaultsKeys.uuid.rawValue)
        
    }
    
    func getUUID() -> String{
        return string(forKey: UserDefaultsKeys.uuid.rawValue) as? String ?? ""
    }

    //Token
    func setToken(value: String){
        set(value, forKey: UserDefaultsKeys.token.rawValue)
        
    }
    
    func getToken() -> String{
        return string(forKey: UserDefaultsKeys.token.rawValue) as? String ?? ""
    }

    //Type Token
    func setTypeToken(value: String){
        set(value, forKey: UserDefaultsKeys.typeToken.rawValue)
        
    }
    
    func getTypeToken() -> String{
        return string(forKey: UserDefaultsKeys.typeToken.rawValue) as? String ?? ""
    }

    //Device
    func setModelDevice(value: String){
        set(value, forKey: UserDefaultsKeys.modelDevice.rawValue)
        
    }
    
    func getModelDevice() -> String{
        return string(forKey: UserDefaultsKeys.modelDevice.rawValue) as? String ?? ""
    }
    
    //Device
    func setSesionTime(value: Bool){
        set(value, forKey: UserDefaultsKeys.sesionTime.rawValue)
    }
    
    func getSesionTime() -> Bool{
        return bool(forKey: UserDefaultsKeys.sesionTime.rawValue)
    }
    
    //IMEI
    func setImeiGuard(value: Bool){
        set(value, forKey: UserDefaultsKeys.imeiGuard.rawValue)
    }
    
    func getImeiGuard() -> Bool{
        return bool(forKey: UserDefaultsKeys.imeiGuard.rawValue)
    }
    
    //Login TRASU
    func setLoginTrasu(value: String){
        set(value, forKey: UserDefaultsKeys.loginTrasu.rawValue)
        
    }
    
    func getLoginTrasu() -> String{
        return string(forKey: UserDefaultsKeys.loginTrasu.rawValue) as? String ?? ""
    }
    
    //Register trasu
    func setRegisterTrasu(value: String){
        set(value, forKey: UserDefaultsKeys.registerTrasu.rawValue)
        
    }
    
    func getRegisterTrasu() -> String{
        return string(forKey: UserDefaultsKeys.registerTrasu.rawValue) as? String ?? ""
    }
    

    
    func setTrasuTiempo(value: Int){
        set(value, forKey: UserDefaultsKeys.tiempoTrasu.rawValue)
        
    }
    
    func getTrasuTiempo() -> Int{
        return string(forKey: UserDefaultsKeys.tiempoTrasu.rawValue) as? Int ?? 0
    }
    
    //APP GLOBAL
    func setAppGlobalPunku(value: String){
        set(value, forKey: UserDefaultsKeys.appGlobalPunku.rawValue)
    }
    
    func getAppGlobalPunku() -> String{
        return string(forKey: UserDefaultsKeys.appGlobalPunku.rawValue) as? String ?? ""
    }
    
    func setAppGlobalCompTel(value: String){
        set(value, forKey: UserDefaultsKeys.appGlobalCompTel.rawValue)
    }
    
    func getAppGlobalCompTel() -> String{
        return string(forKey: UserDefaultsKeys.appGlobalCompTel.rawValue) as? String ?? ""
    }
    
    func setAppGlobalCompMov(value: String){
        set(value, forKey: UserDefaultsKeys.appGlobalCompMov.rawValue)
    }
    
    func getAppGlobalCompMov() -> String{
        return string(forKey: UserDefaultsKeys.appGlobalCompMov.rawValue) as? String ?? ""
    }
    
    func setAppGlobalEnlaceCob(value: String){
        set(value, forKey: UserDefaultsKeys.appGlobalEnlaceCob.rawValue)
    }
    
    func getAppGlobalEnlaceCob() -> String{
        return string(forKey: UserDefaultsKeys.appGlobalEnlaceCob.rawValue) as? String ?? ""
    }

}

enum UserDefaultsKeys : String {
    case isLoggedIn
    case isCalificanos
    case userID
    case name
    case lastnameP
    case lastnameM
    case number
    case email
    case nameComplete
    case uuid
    case opciones
    case token
    case typeToken
    case modelDevice
    case operadorImagenes
    case sesionTime
    case imeiGuard
    case registerTrasu
    case loginTrasu
    case tiempoTrasu
    case appGlobalPunku
    case appGlobalCompTel
    case appGlobalCompMov
    case appGlobalEnlaceCob
}
