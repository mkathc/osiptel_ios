//
//  Connectivity.swift
//  Osiptel_app
//
//  Created by GYS on 5/15/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
