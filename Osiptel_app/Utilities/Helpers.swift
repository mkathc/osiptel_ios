//
//  Helpers.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import UIKit

struct global {

    //Colores
    
    static let BackgroundSB = UIColor(red:0.13, green:0.71, blue:0.85, alpha:1.0)
    static let BackgroundEB = UIColor(red:0.11, green:0.44, blue:0.69, alpha:1.0)
    
    //Mensaje
    static let Titulo = "Error"
    static let Mensaje = "Ha ocurrido un error y el servicio no está disponible temporalmente"
    
    static let ACTION_GETIN_APP = "ACCION_INGRESAR_APP"
    static let ACTION_GETIN_CONSULT_IMEI = "ACCION_INGRESAR_CONSULTA_IMEI"
    static let ACTION_GETIN_OSIPTEL_SIGNAL = "ACCION_INGRESAR_SENAL_OSIPTEL"
    static let ACTION_GETIN_TRASU_PROCEEDINGS = "ACCION_INGRESAR_EXPEDIENTE_TRASU"
    static let ACTION_GETIN_INFORMATION_GUIDE = "ACCION_INGRESAR_GUIA_INFORMACION"
    static let ACTION_GETIN_VERIFY_LINE = "ACCION_INGRESAR_VERIFICAR_LINEA"
    static let ACTION_GETIN_OUR_OFFICES = "ACCION_INGRESAR_NUESTRAS_OFICINAS"
    static let ACTION_GETIN_COMPARATEL = "ACCION_INGRESAR_COMPARATEL"
    static let ACTION_GETIN_COMPARAMOVIL = "ACCION_INGRESAR_COMPARAMOVIL"
    static let ACTION_GETIN_PUNKU = "ACCION_INGRESAR_PUNKU"
    static let ACTION_GETIN_ENTER_RATE_US = "ACCION_INGRESAR_CALIFICANOS"
}



