//
//  ProblemSelectedTableViewCell.swift
//  Osiptel_app
//
//  Created by GYS on 3/31/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class ProblemSelectedTableViewCell: UITableViewCell {
    @IBOutlet weak var lblProblem: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
