//
//  CategoryCollectionViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/4/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageOption: UIImageView!    
    @IBOutlet weak var lblOption: UILabel!
    
}
