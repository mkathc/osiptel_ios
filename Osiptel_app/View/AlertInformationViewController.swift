//
//  InformationViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/25/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class AlertInformationViewController: UIViewController {

    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var titleview: UIView!
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    var visual:UIVisualEffect!
    
    @IBAction func btnCloseAction(_ sender: Any) {
        dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visual = visualEffect.effect
        visualEffect.effect = nil
        titleview.roundedCorners(top: true)
        
        //let tapGesture = UITapGestureRecognizer(target: self, action: Selector("dismissModal"))
        //tapGesture.cancelsTouchesInView = true
        //visualEffect.addGestureRecognizer(tapGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissModal(_:)))
        tapGesture.cancelsTouchesInView = true
        visualEffect.addGestureRecognizer(tapGesture)
        
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        viewAlert.center = visualEffect.center
        viewAlert.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewAlert.alpha = 0
        
        UIView.animate(withDuration: 0.3) {
            self.visualEffect.effect = self.visual
            self.visualEffect.alpha = 1
            self.viewAlert.alpha = 1
            self.viewAlert.transform = CGAffineTransform.identity
            
        }
    }
    
    
    @objc func dismissModal(_ sender: UITapGestureRecognizer) {
       dismiss()
    }

    func dismiss(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.visualEffect.effect = nil
            self.viewAlert.alpha = 0
            self.visualEffect.alpha = 0
            
        }) { (success:Bool) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
