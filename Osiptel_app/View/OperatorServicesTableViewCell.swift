//
//  OperatorServicesTableViewCell.swift
//  Osiptel_app
//
//  Created by GYS on 3/26/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol  OperatorServicesTableViewCellDelegate : class {
    func OperatorServicesSelected(idTipoServicio: Int, TipoServicio: String, state: Bool)
}

class OperatorServicesTableViewCell: UITableViewCell {

    
    var _servicioElement : ServicioOperadorElement?
    weak var delegate : OperatorServicesTableViewCellDelegate?
    var idSelected = [Int]()
    
    
    func setServicio(servicio: ServicioOperadorElement, idSelected: [Int]){
        _servicioElement = servicio
        if let indexOfA = idSelected.index(of: (_servicioElement?.idServicio)!){
            checkButton.isSelected = true
            print("Tipo de servici")
            print(self._servicioElement?.idServicio)
            self.delegate?.OperatorServicesSelected(idTipoServicio: self._servicioElement!.idServicio, TipoServicio: self.lblServices.text!, state: true)
        }else{
            checkButton.isSelected = false
            print("Tipo de sertextvici")
            print(self._servicioElement?.idServicio)
            self.delegate?.OperatorServicesSelected(idTipoServicio: self._servicioElement!.idServicio, TipoServicio: self.lblServices.text!, state: false)
        }
        
        lblServices.text = _servicioElement?.servicio
    }
    
    @IBOutlet weak var checkButton: UIButton!

    @IBAction func checkServices(_ sender: UIButton) {
        print("check \(checkButton.isSelected) ")
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }) { (success) in
                sender.isSelected = !sender.isSelected
                UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
                    sender.transform = .identity
                        self.delegate?.OperatorServicesSelected(idTipoServicio: self._servicioElement!.idServicio, TipoServicio: self.lblServices.text!, state: sender.isSelected)
                }, completion: nil)
        }
    }
    @IBOutlet weak var lblServices: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
