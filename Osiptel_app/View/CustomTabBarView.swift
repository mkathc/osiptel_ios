//
//  CustomTabBarView.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class CustomTabBarView: UITabBarController, UITabBarControllerDelegate {

    var indexPosition : Int = 0
    var isReport: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], for: .normal)
                
        var arrayOfImageNameForUnselectedState = ["ic_Home","ic_imei_w","ic_Files","ic_Signal","ic_location"]
        var arrayOfImageNameForSelectedState = ["ic_Home","ic_imei_w","ic_Files","ic_Signal","ic_location"]
        
        var nombreTab = ["Inicio","Imei","Expedientes","Señal osiptel","Oficinas"]
        
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                let imageNameForSelectedState   = arrayOfImageNameForSelectedState[i]
                let imageNameForUnselectedState = arrayOfImageNameForUnselectedState[i]
                
                self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].accessibilityLabel = nombreTab[i]
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForUnselectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        // Background Tab Selected
        let numberOfItems = CGFloat(tabBar.items!.count)
        
        print("tabbar \(tabBar.frame.height)")
        
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor(red:0.00, green:0.26, blue:0.47, alpha:1.0) , size: tabBarItemSize)
        
        // initaial tab bar index
        self.selectedIndex = indexPosition
        
        /*if isReport{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ReportCoverageViewController") as! ReportCoverageViewController

            self.present(controller, animated: false, completion: nil)
        }*/
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let tabBarIndex = tabBarController.selectedIndex
        
        switch tabBarIndex {
        case 0:
            let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
            rootView.popToRootViewController(animated: false)
            break
            
        case 1:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_CONSULT_IMEI)
            break

        case 2:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_TRASU_PROCEEDINGS)
            break

        case 3:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_OSIPTEL_SIGNAL)
            break

        case 4:
            attempFetchCreateEventoLog(withId: global.ACTION_GETIN_OUR_OFFICES)
            break
        default:
            break
        }
    }

    func attempFetchCreateEventoLog(withId accion: String){
        
        let sistemaOperativo = "iOS"
        let marcaCelular = "Apple"
        let modeloCelular = UIDevice.current.modelName
        let MAC = UserDefaults.standard.getUUID()
        var nombreUsuario = ""
        var numeroCelular = ""
        if(UserDefaults.standard.getNameComplete() != nil){
            nombreUsuario = UserDefaults.standard.getUserID()
            numeroCelular = UserDefaults.standard.getNumber()
        }
        
        let PostB = BodyEntityLog.init(sistemaOperativo: sistemaOperativo, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, accion: accion, mac: MAC, nombreUsuario: nombreUsuario)

        DataService.shared.requestFetchCreateEventoLog(with: PostB) {[weak self] (trasu, error, String) in
            if error != nil {
                print("error")
                print(error)
            }

            if trasu == "Ok"{
                print("CreateEventoLog OK")
            }else if trasu == "error"{
               print("CreateEventoLog ERROR")
            }
        }
    }
}



extension UIImage{
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        var other = false
        
        //Cargar segun dispositivos
        if UIDevice().userInterfaceIdiom == .phone {
            
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                other = true
            case 2688:
                other = true
            case 1792:
                other = true
            default:
                other = false
            }
        }
        
        if other {
            if #available(iOS 11.0, *) {
                sizeThatFits.height = window.safeAreaInsets.bottom + 40
            }
        }
        return sizeThatFits
    }
}
