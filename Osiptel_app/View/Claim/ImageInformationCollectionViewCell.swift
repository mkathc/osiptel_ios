//
//  ImageInformationCollectionViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/15/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class ImageInformationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
