//
//  InformationCollectionViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/5/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class InformationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageInformation: UIImageView!
    @IBOutlet weak var lblInformation: UILabel!
}
