//
//  ServicesSendTableViewCell.swift
//  Osiptel_app
//
//  Created by GYS on 3/26/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol servicesSendDelegate:class {
    func SenderServiceTableViewCellDidLocalidad()
}

class ServicesSendTableViewCell: UITableViewCell {
    
    weak var delegate: servicesSendDelegate?
    
    @IBAction func btnContinueAction(_ sender: Any) {
        delegate?.SenderServiceTableViewCellDidLocalidad()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
