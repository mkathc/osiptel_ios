//
//  SendFilterTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/25/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol SenderFilterTableViewCellDelegate : class {
    func SenderFilterTableViewCellDidLocalidad()
}

class SendFilterTableViewCell: UITableViewCell {

    weak var delegate: SenderFilterTableViewCellDelegate?
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBAction func btnFilterAction(_ sender: Any) {
        //delegate?.SenderFilterTableViewCellDelegate()
        delegate?.SenderFilterTableViewCellDidLocalidad()
    }
    override func awakeFromNib() {
        super.awakeFromNib()        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
