//
//  ReportCoverageTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/5/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol ReportCoverageTableViewCellDelegate : class {
    func ReportCoverageNavigation()
}

class ReportCoverageTableViewCell: UITableViewCell {

    
    var delegate : ReportCoverageTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func btnReportCoverageAction(_ sender: Any) {
        delegate?.ReportCoverageNavigation()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
