//
//  FilterTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/25/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import Foundation

protocol FilterTableViewCellDelegate : class {
    func FilterTableViewCellDidProvincia(idDepartamento: String)
    func FilterTableViewCellDidDistrito(idDepartamento: String,idProvincia: String)
    func FilterTableViewCellDidLocalidad(idDepartamento: String, idProvincia: String, idDistrito: String)
    func FilterTableViewCellDidSeleccion(Departamento: String, Provincia: String, Distrito: String, Localidad: String)
}

class FilterTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
   
    func SendSelected(_ controller: FilterCoverageViewController, departamento: Bool, provincia: Bool, distrito: Bool, localidad: Bool) {

        if departamento{
            txtDepartment.layer.borderColor = myColor.cgColor
            txtDepartment.layer.cornerRadius = 4
            txtDepartment.borderColorView = UIColor.red
            txtDepartment.layer.borderWidth = 1.0
        }else if provincia{
            txtProvince.layer.borderColor = myColor.cgColor
            txtProvince.layer.cornerRadius = 4
            txtProvince.borderColorView = UIColor.red
            txtProvince.layer.borderWidth = 1.0
        }else if distrito{
            txtDistrite.layer.borderColor = myColor.cgColor
            txtDistrite.layer.cornerRadius = 4
            txtDistrite.borderColorView = UIColor.red
            txtDistrite.layer.borderWidth = 1.0
        }else if localidad{
            txtLocalidad.layer.borderColor = myColor.cgColor
            txtLocalidad.layer.cornerRadius = 4
            txtLocalidad.borderColorView = UIColor.red
            txtLocalidad.layer.borderWidth = 1.0
        }else{
            txtDepartment.layer.borderWidth = 0.0
            txtProvince.layer.borderWidth = 0.0
            txtDistrite.layer.borderWidth = 0.0
            txtLocalidad.layer.borderWidth = 0.0            
        }
    }

    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var txtProvince: UITextField!
    @IBOutlet weak var txtDistrite: UITextField!
    @IBOutlet weak var txtLocalidad: UITextField!
    weak var delegate: FilterTableViewCellDelegate?
    private var PickerDepartamento : UIPickerView?
    private var PickerProvincia : UIPickerView?
    private var PickerDistrito : UIPickerView?
    private var PickerLocalizacion : UIPickerView?

    var DepartamentoItem = Departamento()
    var ProvinciaItem = Provincia()
    var DistritoItem = Distrito()
    var LocalidadItem = Localidad()
    var idDepartamento = ""
    var idProvincia = ""
    var idDistrito = ""
    var idLocalidad = ""
    var CountItem = 0
    var idrow = 0
    let myColor = UIColor.red
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        PickerDepartamento = UIPickerView()
        PickerDepartamento?.delegate = self
        PickerDepartamento?.dataSource = self
        PickerDepartamento!.tag = 0
        PickerProvincia = UIPickerView()
        PickerProvincia?.delegate = self
        PickerProvincia?.dataSource = self
        PickerProvincia!.tag = 1
        PickerDistrito = UIPickerView()
        PickerDistrito?.delegate = self
        PickerDistrito?.dataSource = self
        PickerDistrito!.tag = 2
        PickerLocalizacion = UIPickerView()
        PickerLocalizacion?.delegate = self
        PickerLocalizacion?.dataSource = self
        PickerLocalizacion!.tag = 3
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
        toolBar.sizeToFit()
        
        //Adding
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtDepartment.inputAccessoryView = toolBar
        txtProvince.inputAccessoryView = toolBar
        txtDistrite.inputAccessoryView = toolBar
        txtLocalidad.inputAccessoryView = toolBar
        txtDepartment.inputView = PickerDepartamento
        txtProvince.inputView = PickerProvincia
        txtDistrite.inputView = PickerDistrito
        txtLocalidad.inputView = PickerLocalizacion
        txtProvince.isEnabled = false
        txtDistrite.isEnabled = false
        txtLocalidad.isEnabled = false
        
        txtDepartment.text = "Seleccione"
        
    }

    func setDepartamento(departamento: Departamento){
        DepartamentoItem = departamento
    }
    
    func setProvincia(provincia: Provincia){
        ProvinciaItem = provincia
    }
    
    func setDistrito(distrito: Distrito){
        DistritoItem = distrito
    }
    
    func setLocalidad(localidad: Localidad){
        LocalidadItem = localidad
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @objc func doneClick(_ sender : UIPickerView){
        
        if idrow == 0{
            delegate?.FilterTableViewCellDidProvincia(idDepartamento: idDepartamento)
            PickerProvincia?.reloadAllComponents()
            PickerDistrito?.reloadAllComponents()
            PickerLocalizacion?.reloadAllComponents()
        }
        
        if idrow == 1{
            delegate?.FilterTableViewCellDidDistrito(idDepartamento: idDepartamento,idProvincia: idProvincia)
            PickerDistrito?.reloadAllComponents()
            PickerLocalizacion?.reloadAllComponents()
        }
        
        if idrow == 2{
            delegate?.FilterTableViewCellDidLocalidad(idDepartamento: idDepartamento, idProvincia: idProvincia, idDistrito: idDistrito)
            PickerLocalizacion?.reloadAllComponents()
        }
        
        if idrow == 3{
            delegate?.FilterTableViewCellDidSeleccion(Departamento: idDepartamento , Provincia: idProvincia , Distrito: idDistrito , Localidad: idLocalidad )
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 0{
            return DepartamentoItem.count
        }
        
        if pickerView.tag == 1{
            return ProvinciaItem.count
        }
        
        if pickerView.tag == 2{
            return DistritoItem.count
        }

        if pickerView.tag == 3{
            return LocalidadItem.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if  pickerView.tag == 0{
            return DepartamentoItem[row].departamento
        }
        if pickerView.tag == 1{
            return ProvinciaItem[row].provincia
        }
        if pickerView.tag == 2{
            return DistritoItem[row].distrito
        }
        if pickerView.tag == 3{
            return LocalidadItem[row].localidad
        }
        
        return ""
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        idrow = pickerView.tag;
        if pickerView.tag == 0{
            if DepartamentoItem.count > 0{
                txtDepartment.text = DepartamentoItem[row].departamento
                idDepartamento = DepartamentoItem[row].departamento
                txtProvince.isEnabled = true
                txtDistrite.isEnabled = false
                txtLocalidad.isEnabled = false
                txtDistrite.text = ""
                txtLocalidad.text = ""
                txtProvince.text = "Seleccione"
                txtDepartment.layer.borderWidth = 0.0
                txtProvince.layer.borderWidth = 0.0
                txtDistrite.layer.borderWidth = 0.0
                txtLocalidad.layer.borderWidth = 0.0
               
            }
        }
        if pickerView.tag == 1{
            if ProvinciaItem.count > 1{
                txtProvince.text = ProvinciaItem[row].provincia
                idProvincia = ProvinciaItem[row].provincia
                txtDistrite.isEnabled = true
                txtLocalidad.isEnabled = false
                txtDepartment.layer.borderWidth = 0.0
                txtProvince.layer.borderWidth = 0.0
                txtDistrite.layer.borderWidth = 0.0
                txtLocalidad.layer.borderWidth = 0.0
                txtLocalidad.text = ""
                txtDistrite.text = "Seleccione"
               
            }
        }
        if pickerView.tag == 2{
            if DistritoItem.count > 1{
                txtDistrite.text = DistritoItem[row].distrito
                idDistrito = DistritoItem[row].distrito
            }
            
            txtLocalidad.isEnabled = true
            txtDepartment.layer.borderWidth = 0.0
            txtProvince.layer.borderWidth = 0.0
            txtDistrite.layer.borderWidth = 0.0
            txtLocalidad.layer.borderWidth = 0.0
            txtLocalidad.text = "Todos"
        }
        
        if pickerView.tag == 3{
            if LocalidadItem.count > 1{
                txtLocalidad.text = LocalidadItem[row].localidad
                idLocalidad = LocalidadItem[row].localidad
            }
            
        }
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

