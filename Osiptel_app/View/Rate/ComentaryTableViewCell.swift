//
//  ComentaryTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/18/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol comentaryDelegate: class {
    
}

class ComentaryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblComentary: UILabel!
    @IBOutlet weak var txtComentary: UITextView!
    weak var delegate : comentaryDelegate?
    var PreguntaItem : PreguntaElement!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setRate(pregunta: PreguntaElement){
        PreguntaItem = pregunta
        lblComentary.text = PreguntaItem.descripcion 
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
}
