//
//  RateUsTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/18/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol RateUsTableViewCellDelegate : class {
    func RateUsTableViewCellDidTapHeart(idPregunta: Int, respuesta: String)
    func RateUsTableViewCellDidTapTrash(idPregunta: Int, respuesta: String)
}

class RateUsTableViewCell: UITableViewCell {
   
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var LblQuestion: UILabel!
    weak var delegate: RateUsTableViewCellDelegate?
    var PreguntaItem: PreguntaElement!
    var idPregunta : Int!
    
    func setRate(pregunta: PreguntaElement){
        PreguntaItem = pregunta
        LblQuestion.text = PreguntaItem.descripcion        
        //btnYes.isSelected = true
        //btnYes.isSelected = false
    }
    
    @IBAction func YesButton(_ sender: UIButton) {
        
        btnYes.isSelected = true
        
        if sender.isSelected{
            
            sender.setImage(UIImage(named: "Si_on"), for: .selected)
            btnNo.setImage(UIImage(named: "No_off"), for: .normal)
            btnNo.isSelected = false
        }else{
            sender.setImage(UIImage(named: "No_on"), for: .selected)
            btnNo.setImage(UIImage(named: "Si_off"), for: .normal)
            btnNo.isSelected = true
        }
        
        idPregunta = PreguntaItem.idPregunta
        delegate?.RateUsTableViewCellDidTapHeart(idPregunta: idPregunta, respuesta: "Si")
        
    }
    
    @IBAction func NoButton(_ sender: UIButton) {
        
        btnNo.isSelected = true
        
        if sender.isSelected{
            
            sender.setImage(UIImage(named: "No_on"), for: .normal)
            btnYes.setImage(UIImage(named: "Si_off"), for: .normal)
            btnYes.isSelected = false
        }else{
            sender.setImage(UIImage(named: "No_on"), for: .normal)
            btnYes.setImage(UIImage(named: "Si_off"), for: .normal)
            btnYes.isSelected = false
        }

        idPregunta = PreguntaItem.idPregunta 
        delegate?.RateUsTableViewCellDidTapHeart(idPregunta: idPregunta, respuesta: "No")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
