//
//  MyFileTableViewCell.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/7/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import AEAccordion

protocol downloadCell: class {
    func downloadAction(idExpediente: String, recurso: String)
}


class MyFileTableViewCell: AccordionTableViewCell {
    
    static let reuseIdentifier = "MyFileTableviewCell"

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var arrow: UIImageView! {
        didSet {
            arrow.tintColor = UIColor.white
        }
    }
    weak var delegate: downloadCell?
    
    @IBOutlet weak var lblEstado: UILabel!
    @IBOutlet weak var lblNroReclamo: UILabel!
    @IBOutlet weak var lblEmpresaOperadora: UILabel!
    @IBOutlet weak var btnDescargar: UIButton!    
    @IBOutlet weak var detailView: UIView!
    
    var ExpedienteItem: ExpedienteElement!
    
    @IBAction func btnDescargarAction(_ sender: Any) {
        delegate?.downloadAction(idExpediente: ExpedienteItem.idExpediente, recurso: ExpedienteItem.recurso)
    }
    //@IBOutlet weak var FileView: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setFile(expediente: ExpedienteElement){
        ExpedienteItem = expediente        
        lblEstado.text = ExpedienteItem.estado
        var nroReclamo = ExpedienteItem.nroReclamo
        if let nroReclamoString = nroReclamo.range(of: "/") {
            nroReclamo.replaceSubrange(nroReclamoString, with: "\n")
        }
        if let nroReclamoString = nroReclamo.range(of: "/ ") {
            nroReclamo.replaceSubrange(nroReclamoString, with: "\n")
        }
        if let nroReclamoString = nroReclamo.range(of: " /") {
            nroReclamo.replaceSubrange(nroReclamoString, with: "\n")
        }
        if let nroReclamoString = nroReclamo.range(of: " / ") {
            nroReclamo.replaceSubrange(nroReclamoString, with: "\n")
        }
        if let nroReclamoString = nroReclamo.range(of: " ") {
            nroReclamo.replaceSubrange(nroReclamoString, with: "\n")
        }
        lblNroReclamo.text = nroReclamo
        
        
        lblEmpresaOperadora.text = ExpedienteItem.empresa
    }
    
    override func setExpanded(_ expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)
        
        if expanded{
            detailView.roundedCorners(top: false)
            headerView.roundedCorners(top: true)
            headerView.backgroundColor = UIColor(red:0.93, green:0.54, blue:0.17, alpha:1.0)
        }else{
            headerView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.86, alpha:1.0)
            headerView.roundedCornersAll()
            
        }
        if animated {
            let alwaysOptions: UIView.AnimationOptions = [.allowUserInteraction,
                                                          .beginFromCurrentState,
                                                          .transitionCrossDissolve]
            let expandedOptions: UIView.AnimationOptions = [.transitionFlipFromTop, .curveEaseOut]
            let collapsedOptions: UIView.AnimationOptions = [.transitionFlipFromBottom, .curveEaseIn]
            let options = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
            UIView.transition(with: detailView, duration: 0.3, options: options, animations: {
                self.toggleCell()
            }, completion: nil)
        } else {
            toggleCell()
        }
    }
    
    // MARK: Helpers
    
    private func toggleCell() {
        detailView.isHidden = !expanded
        arrow.transform = expanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
    }
}
