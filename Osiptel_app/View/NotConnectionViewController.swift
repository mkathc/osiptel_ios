//
//  NotConnectionViewController.swift
//  Osiptel_app
//
//  Created by GYS on 5/14/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
class NotConnectionViewController: UIViewController {
    
    
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var titleview: UIView!
    var visual:UIVisualEffect!
    @IBOutlet weak var viewAlert: UIView!
     @IBOutlet weak var btnCancel: UIButton!
     
    @IBAction func btnCancelAction(_ sender: Any) {
        dismiss()
    }

    @IBAction func btnReintentarAction(_ sender: Any) {
        var isError:Bool
        isError = false
        if(Connectivity.isConnectedToInternet() ){
            dismiss()
        }
        
        //dismiss()
        /*if (isError){
            dismiss()
        }*/
    }
    
    /*
    
    @IBAction func btnAceptarAction(_ sender: Any) {
        UserDefaults.standard.setSesionTime(value: false)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
        
        //try self.tabBarController?.selectedIndex = 2
        controller.selectedIndex = 2
        
        self.present(controller, animated: false, completion: nil)
    }

    */
    override func viewDidLoad() {
        super.viewDidLoad()
        visual = visualEffect.effect
        visualEffect.effect = nil
        titleview.roundedCorners(top: true)
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        viewAlert.center = visualEffect.center
        viewAlert.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewAlert.alpha = 0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissModal(_:)))
        tapGesture.cancelsTouchesInView = true
        visualEffect.addGestureRecognizer(tapGesture)
        
        
        UIView.animate(withDuration: 0.3) {
            self.visualEffect.effect = self.visual
            self.visualEffect.alpha = 1
            self.viewAlert.alpha = 1
            self.viewAlert.transform = CGAffineTransform.identity
        }
        
    }
    
    
    @objc func dismissModal(_ sender: UITapGestureRecognizer){
        dismiss()
    }
    
    func dismiss(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.visualEffect.effect = nil
            self.viewAlert.alpha = 0
            self.visualEffect.alpha = 0
            
        }) { (success:Bool) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
