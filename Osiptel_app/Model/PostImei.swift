//
//  PostImei.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/28/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

struct PostImei: Codable {
    let codigoImei: String
    let empresaOperadora: Int
    let descripcionEmpresaOperadora, nombres, apellidoPaterno, apellidoMaterno: String
    let idMarca: Int
    let descripcionMarca: String
    let idModelo: Int
    let descripcionModelo, numeroMovil, codigoBloqueo, fechaReporte: String
    let horaReporte, minutoReporte, fechaRecuperacion, numeroContacto: String
    let problemaDetectado: Int
    let correoElectronico, guid, comentario, archivo: String
}

// MARK: Convenience initializers and mutators

extension PostImei {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PostImei.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        codigoImei: String? = nil,
        empresaOperadora: Int? = nil,
        descripcionEmpresaOperadora: String? = nil,
        nombres: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        idMarca: Int? = nil,
        descripcionMarca: String? = nil,
        idModelo: Int? = nil,
        descripcionModelo: String? = nil,
        numeroMovil: String? = nil,
        codigoBloqueo: String? = nil,
        fechaReporte: String? = nil,
        horaReporte: String? = nil,
        minutoReporte: String? = nil,
        fechaRecuperacion: String? = nil,
        numeroContacto: String? = nil,
        problemaDetectado: Int? = nil,
        correoElectronico: String? = nil,
        guid: String? = nil,
        comentario: String? = nil,
        archivo: String? = nil
        ) -> PostImei {
        return PostImei(
            codigoImei: codigoImei ?? self.codigoImei,
            empresaOperadora: empresaOperadora ?? self.empresaOperadora,
            descripcionEmpresaOperadora: descripcionEmpresaOperadora ?? self.descripcionEmpresaOperadora,
            nombres: nombres ?? self.nombres,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            idMarca: idMarca ?? self.idMarca,
            descripcionMarca: descripcionMarca ?? self.descripcionMarca,
            idModelo: idModelo ?? self.idModelo,
            descripcionModelo: descripcionModelo ?? self.descripcionModelo,
            numeroMovil: numeroMovil ?? self.numeroMovil,
            codigoBloqueo: codigoBloqueo ?? self.codigoBloqueo,
            fechaReporte: fechaReporte ?? self.fechaReporte,
            horaReporte: horaReporte ?? self.horaReporte,
            minutoReporte: minutoReporte ?? self.minutoReporte,
            fechaRecuperacion: fechaRecuperacion ?? self.fechaRecuperacion,
            numeroContacto: numeroContacto ?? self.numeroContacto,
            problemaDetectado: problemaDetectado ?? self.problemaDetectado,
            correoElectronico: correoElectronico ?? self.correoElectronico,
            guid: guid ?? self.guid,
            comentario: comentario ?? self.comentario,
            archivo: archivo ?? self.archivo
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responsePostImei(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PostImei>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
