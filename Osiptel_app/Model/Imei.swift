//
//  Imei.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/28/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

typealias Imei = [ImeiElement]

struct ImeiElement: Codable {
    let idTermMOV: Int
    let ne, fechaHora, fechaHoraArchivo, reporte: String
    let pais, codOPerador, operador, tecnologia: String
    
    enum CodingKeys: String, CodingKey {
        case idTermMOV = "idTermMov"
        case ne, fechaHora, fechaHoraArchivo, reporte, pais, codOPerador, operador, tecnologia
    }
}

// MARK: Convenience initializers and mutators

extension ImeiElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ImeiElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        idTermMOV: Int? = nil,
        ne: String? = nil,
        fechaHora: String? = nil,
        fechaHoraArchivo: String? = nil,
        reporte: String? = nil,
        pais: String? = nil,
        codOPerador: String? = nil,
        operador: String? = nil,
        tecnologia: String? = nil
        ) -> ImeiElement {
        return ImeiElement(
            idTermMOV: idTermMOV ?? self.idTermMOV,
            ne: ne ?? self.ne,
            fechaHora: fechaHora ?? self.fechaHora,
            fechaHoraArchivo: fechaHoraArchivo ?? self.fechaHoraArchivo,
            reporte: reporte ?? self.reporte,
            pais: pais ?? self.pais,
            codOPerador: codOPerador ?? self.codOPerador,
            operador: operador ?? self.operador,
            tecnologia: tecnologia ?? self.tecnologia
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == Imei.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Imei.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseImei(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Imei>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
