//
//  UsuarioPost.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/19/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//


import Foundation
import Alamofire

struct PostUsuario: Codable {
    let tipoDocumento: String
    let numeroDocumento: String
    let nroTelefono: String
    let correoElectronico: String
    let nombre: String
    let apellidoPaterno: String
    let apellidoMaterno: String
    let modeloCelular: String
    let marcaCelular: String
    let mac: String
    let usuarioRegistro: String
    
    enum CodingKeys: String, CodingKey {
        case tipoDocumento = "tipoDocumento"
        case numeroDocumento = "numeroDocumento"
        case nroTelefono = "nroTelefono"
        case correoElectronico = "correoElectronico"
        case nombre = "nombre"
        case apellidoPaterno = "apellidoPaterno"
        case apellidoMaterno = "apellidoMaterno"
        case modeloCelular = "modeloCelular"
        case marcaCelular = "marcaCelular"
        case mac = "mac"
        case usuarioRegistro = "usuarioRegistro"
    }
}

// MARK: Convenience initializers and mutators

extension PostUsuario {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PostUsuario.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        tipoDocumento: String? = nil,
        numeroDocumento: String? = nil,
        nroTelefono: String? = nil,
        correoElectronico: String? = nil,
        nombre: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        modeloCelular: String? = nil,
        marcaCelular: String? = nil,
        mac: String? = nil,
        usuarioRegistro: String? = nil
        ) -> PostUsuario {
        return PostUsuario(
            tipoDocumento: tipoDocumento ?? self.tipoDocumento,
            numeroDocumento: numeroDocumento ?? self.numeroDocumento,
            nroTelefono: nroTelefono ?? self.nroTelefono,
            correoElectronico: correoElectronico ?? self.correoElectronico,
            nombre: nombre ?? self.nombre,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            modeloCelular: modeloCelular ?? self.modeloCelular,
            marcaCelular: marcaCelular ?? self.marcaCelular,
            mac: mac ?? self.mac,
            usuarioRegistro: usuarioRegistro ?? self.usuarioRegistro
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responsePostUsuario(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PostUsuario>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
