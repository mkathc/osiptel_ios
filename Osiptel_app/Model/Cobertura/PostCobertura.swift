//
//  PostCobertura.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/11/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

struct PostCobertura: Codable {
    let nombre, apellidoPaterno, apellidoMaterno, numeroTelefono: String
    let marcaEquipo, modeloEquipo, sistemaOperativo: String
    let idEmpresa: Int
    let fechaReporte, correoElectronico: String
    let idProblema: Int
    let departamento, provincia, distrito, localidad: String
    let latitudReporte, longitudReporte, latitudUbicacionPersona, longitudUbicacionPersona: String
    let idServicio, marcaCelular, modeloCelular, numeroCelular: String
    let mac, nombreUsuario: String
}

// MARK: Convenience initializers and mutators

extension PostCobertura {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PostCobertura.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        nombre: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        numeroTelefono: String? = nil,
        marcaEquipo: String? = nil,
        modeloEquipo: String? = nil,
        sistemaOperativo: String? = nil,
        idEmpresa: Int? = nil,
        fechaReporte: String? = nil,
        correoElectronico: String? = nil,
        idProblema: Int? = nil,
        departamento: String? = nil,
        provincia: String? = nil,
        distrito: String? = nil,
        localidad: String? = nil,
        latitudReporte: String? = nil,
        longitudReporte: String? = nil,
        latitudUbicacionPersona: String? = nil,
        longitudUbicacionPersona: String? = nil,
        idServicio: String? = nil,
        marcaCelular: String? = nil,
        modeloCelular: String? = nil,
        numeroCelular: String? = nil,
        mac: String? = nil,
        nombreUsuario: String? = nil
        ) -> PostCobertura {
        return PostCobertura(
            nombre: nombre ?? self.nombre,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            numeroTelefono: numeroTelefono ?? self.numeroTelefono,
            marcaEquipo: marcaEquipo ?? self.marcaEquipo,
            modeloEquipo: modeloEquipo ?? self.modeloEquipo,
            sistemaOperativo: sistemaOperativo ?? self.sistemaOperativo,
            idEmpresa: idEmpresa ?? self.idEmpresa,
            fechaReporte: fechaReporte ?? self.fechaReporte,
            correoElectronico: correoElectronico ?? self.correoElectronico,
            idProblema: idProblema ?? self.idProblema,
            departamento: departamento ?? self.departamento,
            provincia: provincia ?? self.provincia,
            distrito: distrito ?? self.distrito,
            localidad: localidad ?? self.localidad,
            latitudReporte: latitudReporte ?? self.latitudReporte,
            longitudReporte: longitudReporte ?? self.longitudReporte,
            latitudUbicacionPersona: latitudUbicacionPersona ?? self.latitudUbicacionPersona,
            longitudUbicacionPersona: longitudUbicacionPersona ?? self.longitudUbicacionPersona,
            idServicio: idServicio ?? self.idServicio,
            marcaCelular: marcaCelular ?? self.marcaCelular,
            modeloCelular: modeloCelular ?? self.modeloCelular,
            numeroCelular: numeroCelular ?? self.numeroCelular,
            mac: mac ?? self.mac,
            nombreUsuario: nombreUsuario ?? self.nombreUsuario
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responsePostCobertura(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PostCobertura>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
