//
//  CoberturaMovil.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/4/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

typealias CoberturaMovil = [CoberturaMovilElement]

struct CoberturaMovilElement: Codable {
    let idUbigeo, longitud, latitud, lugar: String
    let departamento, provincia, distrito, localidad: String
    let cobertura, img, nroReporte: String
    let tieneCobertura: Bool
    let tecnologisOperador: [TecnologisOperador]?
}

struct TecnologisOperador: Codable {
    let codEmpresa, empresa, tecnologia: String
    let tecnologiaArray: [String]
}

// MARK: Convenience initializers and mutators

extension CoberturaMovilElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(CoberturaMovilElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        idUbigeo: String? = nil,
        longitud: String? = nil,
        latitud: String? = nil,
        lugar: String? = nil,
        departamento: String? = nil,
        provincia: String? = nil,
        distrito: String? = nil,
        localidad: String? = nil,
        cobertura: String? = nil,
        img: String? = nil,
        nroReporte: String? = nil,
        tieneCobertura: Bool = false,
        tecnologisOperador: [TecnologisOperador]? = nil
        ) -> CoberturaMovilElement {
        return CoberturaMovilElement(
            idUbigeo: idUbigeo ?? self.idUbigeo,
            longitud: longitud ?? self.longitud,
            latitud: latitud ?? self.latitud,
            lugar: lugar ?? self.lugar,
            departamento: departamento ?? self.departamento,
            provincia: provincia ?? self.provincia,
            distrito: distrito ?? self.distrito,
            localidad: localidad ?? self.localidad,
            cobertura: cobertura ?? self.cobertura,
            img: img ?? self.img,
            nroReporte: nroReporte ?? self.nroReporte,
            tieneCobertura: tieneCobertura ?? self.tieneCobertura,
            tecnologisOperador: tecnologisOperador ?? self.tecnologisOperador
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension TecnologisOperador {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(TecnologisOperador.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        codEmpresa: String? = nil,
        empresa: String? = nil,
        tecnologia: String? = nil,
        tecnologiaArray: [String]? = nil
        ) -> TecnologisOperador {
        return TecnologisOperador(
            codEmpresa: codEmpresa ?? self.codEmpresa,
            empresa: empresa ?? self.empresa,
            tecnologia: tecnologia ?? self.tecnologia,
            tecnologiaArray: tecnologiaArray ?? self.tecnologiaArray
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == CoberturaMovil.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(CoberturaMovil.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseCoberturaMovil(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<CoberturaMovil>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
