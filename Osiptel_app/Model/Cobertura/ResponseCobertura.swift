//
//  ResponseCobertura.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/11/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

struct ResponseCobertura: Codable {
    let flgSist, departamento, provincia, distrito: String
    let localidad, nombre, apellidoPaterno, apellidoMaterno: String
    let dni, email, numeroTelefono, marcaEquipo: String
    let otraMarca, modeloEquipo, otroModelo, soEquipo: String
    let otroSO, fechaOcurrencia, empresaOperadora, reaLLam: String
    let recLlam, lugarLlam, otroLugar, calidadLlam: String
    let otroCalidad, intensidad, comentario: String
    let latitud, longitud: Int
}

// MARK: Convenience initializers and mutators

extension ResponseCobertura {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ResponseCobertura.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        flgSist: String? = nil,
        departamento: String? = nil,
        provincia: String? = nil,
        distrito: String? = nil,
        localidad: String? = nil,
        nombre: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        dni: String? = nil,
        email: String? = nil,
        numeroTelefono: String? = nil,
        marcaEquipo: String? = nil,
        otraMarca: String? = nil,
        modeloEquipo: String? = nil,
        otroModelo: String? = nil,
        soEquipo: String? = nil,
        otroSO: String? = nil,
        fechaOcurrencia: String? = nil,
        empresaOperadora: String? = nil,
        reaLLam: String? = nil,
        recLlam: String? = nil,
        lugarLlam: String? = nil,
        otroLugar: String? = nil,
        calidadLlam: String? = nil,
        otroCalidad: String? = nil,
        intensidad: String? = nil,
        comentario: String? = nil,
        latitud: Int? = nil,
        longitud: Int? = nil
        ) -> ResponseCobertura {
        return ResponseCobertura(
            flgSist: flgSist ?? self.flgSist,
            departamento: departamento ?? self.departamento,
            provincia: provincia ?? self.provincia,
            distrito: distrito ?? self.distrito,
            localidad: localidad ?? self.localidad,
            nombre: nombre ?? self.nombre,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            dni: dni ?? self.dni,
            email: email ?? self.email,
            numeroTelefono: numeroTelefono ?? self.numeroTelefono,
            marcaEquipo: marcaEquipo ?? self.marcaEquipo,
            otraMarca: otraMarca ?? self.otraMarca,
            modeloEquipo: modeloEquipo ?? self.modeloEquipo,
            otroModelo: otroModelo ?? self.otroModelo,
            soEquipo: soEquipo ?? self.soEquipo,
            otroSO: otroSO ?? self.otroSO,
            fechaOcurrencia: fechaOcurrencia ?? self.fechaOcurrencia,
            empresaOperadora: empresaOperadora ?? self.empresaOperadora,
            reaLLam: reaLLam ?? self.reaLLam,
            recLlam: recLlam ?? self.recLlam,
            lugarLlam: lugarLlam ?? self.lugarLlam,
            otroLugar: otroLugar ?? self.otroLugar,
            calidadLlam: calidadLlam ?? self.calidadLlam,
            otroCalidad: otroCalidad ?? self.otroCalidad,
            intensidad: intensidad ?? self.intensidad,
            comentario: comentario ?? self.comentario,
            latitud: latitud ?? self.latitud,
            longitud: longitud ?? self.longitud
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseResponseCobertura(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseCobertura>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}

