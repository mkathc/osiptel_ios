//
//  PostRegistroTrasu.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/12/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

struct PostRegistroTrasu: Codable {
    let tipoDocumento, numeroDocumento, nombres, apellidoPaterno: String
    let apellidoMaterno, correoElectronico, confirmarCorreoElectronico: String
    let idServicioReclamado, idCodigoAsociado: Int
    let numeroMovil, contraseña, confirmarContraseña: String
}

// MARK: Convenience initializers and mutators

extension PostRegistroTrasu {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PostRegistroTrasu.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        tipoDocumento: String? = nil,
        numeroDocumento: String? = nil,
        nombres: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        correoElectronico: String? = nil,
        confirmarCorreoElectronico: String? = nil,
        idServicioReclamado: Int? = nil,
        idCodigoAsociado: Int? = nil,
        numeroMovil: String? = nil,
        contraseña: String? = nil,
        confirmarContraseña: String? = nil
        ) -> PostRegistroTrasu {
        return PostRegistroTrasu(
            tipoDocumento: tipoDocumento ?? self.tipoDocumento,
            numeroDocumento: numeroDocumento ?? self.numeroDocumento,
            nombres: nombres ?? self.nombres,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            correoElectronico: correoElectronico ?? self.correoElectronico,
            confirmarCorreoElectronico: confirmarCorreoElectronico ?? self.confirmarCorreoElectronico,
            idServicioReclamado: idServicioReclamado ?? self.idServicioReclamado,
            idCodigoAsociado: idCodigoAsociado ?? self.idCodigoAsociado,
            numeroMovil: numeroMovil ?? self.numeroMovil,
            contraseña: contraseña ?? self.contraseña,
            confirmarContraseña: confirmarContraseña ?? self.confirmarContraseña
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responsePostRegistroTrasu(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PostRegistroTrasu>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
