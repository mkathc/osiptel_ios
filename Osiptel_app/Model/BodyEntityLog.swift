//
//  BodyEntityLog.swift
//  Osiptel_app
//
//  Created by GYS on 2/24/20.
//  Copyright © 2020 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

struct BodyEntityLog: Codable {
    let sistemaOperativo: String
    let marcaCelular: String
    let modeloCelular: String
    let numeroCelular: String
    let accion: String
    let mac: String
    let nombreUsuario: String
    
    enum CodingKeys: String, CodingKey {
        case sistemaOperativo = "sistemaOperativo"
        case marcaCelular = "marcaCelular"
        case modeloCelular = "modeloCelular"
        case numeroCelular = "numeroCelular"
        case accion = "accion"
        case mac = "mac"
        case nombreUsuario = "nombreUsuario"
    }
}

extension BodyEntityLog {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(BodyEntityLog.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        
        sistemaOperativo: String? = nil,
        marcaCelular: String? = nil,
        modeloCelular: String? = nil,
        numeroCelular: String? = nil,
        accion: String? = nil,
        mac: String? = nil,
        nombreUsuario: String? = nil
        ) -> BodyEntityLog {
        return BodyEntityLog(
            sistemaOperativo: sistemaOperativo ?? self.sistemaOperativo,
            marcaCelular: marcaCelular ?? self.marcaCelular,
            modeloCelular: modeloCelular ?? self.modeloCelular,
            numeroCelular: numeroCelular ?? self.numeroCelular,
            accion: accion ?? self.accion,
            mac: mac ?? self.mac,
            nombreUsuario: nombreUsuario ?? self.nombreUsuario
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responsePostUsuario(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<BodyEntityLog>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}

