//
//  Usuario.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/19/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import Foundation
import Alamofire

typealias Usuario = [UsuarioElement]

struct UsuarioElement: Codable {
    let idUsuario: Int
    let tipoDocumento, nroDocumento, nroTelefono, nombre, apellidoPaterno, apellidoMaterno: String
    let modeloCelular, marcaCelular, correoElectronico, mac: String
}

// MARK: Convenience initializers and mutators

extension UsuarioElement {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(UsuarioElement.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        idUsuario: Int? = nil,
        tipoDocumento: String? = nil,
        nroDocumento: String? = nil,
        nroTelefono: String? = nil,
        nombre: String? = nil,
        apellidoPaterno: String? = nil,
        apellidoMaterno: String? = nil,
        modeloCelular: String? = nil,
        marcaCelular: String? = nil,
        correoElectronico: String? = nil,
        mac: String? = nil
        ) -> UsuarioElement {
        return UsuarioElement(
            idUsuario: idUsuario ?? self.idUsuario,
            tipoDocumento: tipoDocumento ?? self.tipoDocumento,
            nroDocumento: nroDocumento ?? self.nroDocumento,
            nroTelefono: nroTelefono ?? self.nroTelefono,
            nombre: nombre ?? self.nombre,
            apellidoPaterno: apellidoPaterno ?? self.apellidoPaterno,
            apellidoMaterno: apellidoMaterno ?? self.apellidoMaterno,
            modeloCelular: modeloCelular ?? self.modeloCelular,
            marcaCelular: marcaCelular ?? self.marcaCelular,
            correoElectronico: correoElectronico ?? self.correoElectronico,
            mac: mac ?? self.mac
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == Usuario.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Usuario.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

fileprivate func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

fileprivate func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseUsuario(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Usuario>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
