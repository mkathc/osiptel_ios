//
//  ReportControllerExtendViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/7/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ReportControllerExtendViewController: UIViewController,NVActivityIndicatorViewable {

    @IBAction func closeViewAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtApellidoPaterno: UITextField!
    @IBOutlet weak var txtApellidoMaterno: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtOperadora: UITextField!
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtProblema: UITextField!
    private var PickerOperadora : UIPickerView?
    private var PickerFecha : UIDatePicker?
    @IBOutlet weak var infoButton: UIImageView!
    @IBOutlet weak var BlurEffect: UIVisualEffectView!
    
    
    
    var idEmpresa: Int = 0
    var idTipoProblema: Int = 0
    var isValidate = false
    
    
    //Model
    var Departamento : String = ""
    var Provincia : String = ""
    var Distrito : String = ""
    var localidad : String = ""
    var Latitud : Double = 0
    var Longitud : Double = 0
    
    //
    private var PickertipoProblema : UIPickerView?
    var visualEffect:UIVisualEffect!
    var _empresa = Empresa()
    var _tipoProblema = TipoProblema()
    var postCobertura : PostCobertura!
    
    
    //
    let IP = "1"
    let numeroCelular = UserDefaults.standard.getNumber()
    let MAC = UserDefaults.standard.getUUID()
    let marcaCelular = "Apple"
    let sistemaOperativo = "iOS"
    let modeloCelular = UserDefaults.standard.getModelDevice()
    let nombreUsuario = UserDefaults.standard.getNameComplete()
    
    let myColor = UIColor.red
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        
        txtNombres.delegate = self
        txtNombres.tag = 0
        txtApellidoPaterno.delegate = self
        txtApellidoPaterno.tag = 1
        txtApellidoMaterno.delegate = self
        txtApellidoMaterno.tag = 2
        txtTelefono.delegate = self
        txtTelefono.tag = 3
        PickerOperadora = UIPickerView()
        PickerOperadora?.delegate = self
        PickerOperadora?.dataSource = self
        PickerOperadora?.tag = 4
        txtOperadora.inputView = PickerOperadora
        txtFecha.delegate = self
        PickerFecha?.tag = 5
        
        PickerFecha = UIDatePicker()
        PickerFecha?.datePickerMode = .date
        PickerFecha?.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        txtFecha?.inputView =  PickerFecha
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
        let infoTap = UITapGestureRecognizer(target: self, action: #selector(tapInfo))
        infoButton.isUserInteractionEnabled = true
        infoButton.addGestureRecognizer(infoTap)
        
        PickertipoProblema = UIPickerView()
        PickertipoProblema?.delegate = self
        PickertipoProblema?.dataSource = self
        PickertipoProblema?.tag = 7
        txtProblema.inputView = PickertipoProblema
        
        txtCorreo.delegate = self
        txtCorreo.tag = 6
        attempFetchEmpresa()
        attempFetchTipoProblema()
        
    }
    
    @objc func tapInfo(sender:UITapGestureRecognizer) {
        alertInformationViewController()
    }
    
    func attempFetchEmpresa(){
        DataService.shared.requestFetchEmpresa{ [weak self] (empresa, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let empresa = empresa else{
                return
            }
            self?.updateEmpresa(with: empresa)
        }
    }
    
    func updateEmpresa(with empresa: Empresa) {
        _empresa = empresa
        _empresa.insert(EmpresaElement.init(codEmpresa: "0", empresa: "Seleccionar"), at: 0)
        PickerOperadora?.reloadAllComponents()
    }
    
    
    func attempFetchTipoProblema(){
        DataService.shared.requestFetchTipoProblema{ [weak self] (tipoProblema, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let tipoProblema = tipoProblema else{
                return
            }
            self?.updateTipoProblema(with: tipoProblema)
        }
    }
    
    func updateTipoProblema(with tipoProblema: TipoProblema) {
        _tipoProblema = tipoProblema
        _tipoProblema.insert(TipoProblemaElement.init(idTipoProblema: 0, tipoProblema: "Seleccionar"), at: 0)
        PickertipoProblema?.reloadAllComponents()
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        txtFecha.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
        
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    @IBAction func btnReportAction(_ sender: Any) {
        
        isValidate = true
        
        if txtNombres.text?.count == 0{
            isValidate = false
            isValidate = false
            txtNombres.borderStyle = UITextBorderStyle.roundedRect
            txtNombres.layer.borderColor = myColor.cgColor
            txtNombres.layer.cornerRadius = 4
            txtNombres.borderColorView = UIColor.red
            txtNombres.layer.borderWidth = 1.0
            //txtNombres
        }
        
        if txtApellidoPaterno.text?.count == 0{
            isValidate = false
            txtApellidoPaterno.borderStyle = UITextBorderStyle.roundedRect
            txtApellidoPaterno.layer.borderColor = myColor.cgColor
            txtApellidoPaterno.layer.cornerRadius = 4
            txtApellidoPaterno.borderColorView = UIColor.red
            txtApellidoPaterno.layer.borderWidth = 1.0
            
        }
        
        if txtApellidoMaterno.text?.count == 0{
            isValidate = false
            txtApellidoMaterno.borderStyle = UITextBorderStyle.roundedRect
            txtApellidoMaterno.layer.borderColor = myColor.cgColor
            txtApellidoMaterno.layer.cornerRadius = 4
            txtApellidoMaterno.borderColorView = UIColor.red
            txtApellidoMaterno.layer.borderWidth = 1.0
        }
        
        if txtTelefono.text?.count == 0{
            isValidate = false
            txtTelefono.borderStyle = UITextBorderStyle.roundedRect
            txtTelefono.layer.borderColor = myColor.cgColor
            txtTelefono.layer.cornerRadius = 4
            txtTelefono.borderColorView = UIColor.red
            txtTelefono.layer.borderWidth = 1.0
        }
        
        if txtFecha.text?.count == 0{
            isValidate = false
            txtFecha.borderStyle = UITextBorderStyle.roundedRect
            txtFecha.layer.borderColor = myColor.cgColor
            txtFecha.layer.cornerRadius = 4
            txtFecha.borderColorView = UIColor.red
            txtFecha.layer.borderWidth = 1.0
            
        }
        
        if txtCorreo.text?.count == 0{
            isValidate = false
            txtCorreo.borderStyle = UITextBorderStyle.roundedRect
            txtCorreo.layer.borderColor = myColor.cgColor
            txtCorreo.layer.cornerRadius = 4
            txtCorreo.borderColorView = UIColor.red
            txtCorreo.layer.borderWidth = 1.0
        }
        
        if txtOperadora.text?.count == 0{
            isValidate = false
            txtOperadora.borderStyle = UITextBorderStyle.roundedRect
            txtOperadora.layer.borderColor = myColor.cgColor
            txtOperadora.layer.cornerRadius = 4
            txtOperadora.borderColorView = UIColor.red
            txtOperadora.layer.borderWidth = 1.0
        }
        
        if txtProblema.text?.count == 0{
            isValidate = false
            txtProblema.borderStyle = UITextBorderStyle.roundedRect
            txtProblema.layer.borderColor = myColor.cgColor
            txtProblema.layer.cornerRadius = 4
            txtProblema.borderColorView = UIColor.red
            txtProblema.layer.borderWidth = 1.0
        }

        if isValidate{
            
            /*let _post = PostCobertura.init(nombre: txtNombres.text!, apellidoPaterno: txtApellidoPaterno.text!, apellidoMaterno: txtApellidoPaterno.text!, numeroTelefono: txtTelefono.text!, marcaEquipo: "Apple", modeloEquipo: UserDefaults.standard.getModelDevice(), sistemaOperativo: "iOS", idEmpresa: idEmpresa, fechaReporte: txtFecha.text!, correoElectronico: txtCorreo.text!, idProblema: idTipoProblema, departamento: Departamento, provincia: Provincia, distrito: Distrito, localidad: localidad, latitudReporte: Latitud, longitudReporte: Longitud, latitudUbicacionPersona: 0, longitudUbicacionPersona: 0, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, mac: MAC, nombreUsuario: nombreUsuario)
      
            attempFetchPostCobertura(withId: _post)
            */
        }
    }
    
    func attempFetchPostCobertura(withId post: PostCobertura){
        let size = CGSize(width: 308, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        DataService.shared.requestFetchCreateReporteCobertura(with: post) {[weak self] (response, error, String, Mensaje) in
            print("Response \(response)")
            self?.stopAnimating()
            
            if String == "200"{
                self!.alertViewController(titulo: "", mensaje: "Se realizo el reporte de cobertura", closeFlag: false, imageValidate: true, closeAutomatic: true, sendVC: "CustomTabBarView")
            }
            
            if let error = error {
                self!.alertViewController(titulo: "", mensaje: "Intente nuevamente", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
                return
            }

        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ReportControllerExtendViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField.tag == 0{
            txtNombres.layer.borderWidth = 0
        }
        
        if textField.tag == 1{
            txtApellidoPaterno.layer.borderWidth = 0
        }
        
        if textField.tag == 2{
            txtApellidoMaterno.layer.borderWidth = 0
        }
        
        if textField.tag == 3{
            txtTelefono.layer.borderWidth = 0
        }
        
        if textField.tag == 5{
            txtFecha.layer.borderWidth = 0
        }
        
        if textField.tag == 6{
            txtCorreo.layer.borderWidth = 0
        }
        
        if textField.tag == 7{
            txtProblema.layer.borderWidth = 0
        }

        
        
        if textField.tag == 3{
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField.tag == 0 || textField.tag == 1 || textField.tag == 2{
            
            let allowedCharacters = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
            
        }
        
        return true
    }
    
}
extension ReportControllerExtendViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 4{
            return _empresa.count
        }
        if pickerView.tag == 7{
            return _tipoProblema.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 4{
            return _empresa[row].empresa
        }
        
        if pickerView.tag == 7{
            return _tipoProblema[row].tipoProblema
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 4{
            txtOperadora.text = _empresa[row].empresa
            idEmpresa = Int(_empresa[row].codEmpresa)!
        }
        if pickerView.tag == 7{
            txtProblema.text = _tipoProblema[row].tipoProblema
            idTipoProblema = _tipoProblema[row].idTipoProblema
        }
    }
}
