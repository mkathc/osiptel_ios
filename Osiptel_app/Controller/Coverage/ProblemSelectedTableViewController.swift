//
//  ProblemSelectedTableViewController.swift
//  Osiptel_app
//
//  Created by GYS on 3/31/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

protocol selectedProblemDelegate: class {
    func sendProblem(idproblema : String ,problema: String)
}

class ProblemSelectedTableViewController: UITableViewController {

    var _problema = Problema()
    var _tipoServicio = Servicio()
    weak var delegate: selectedProblemDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if _problema.count > 0 {
            title = "Selecciona un problema"
        }else{
            title = "Selecciona un tipo de servicio"
        }
        
        
        tableView.reloadData()
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if _problema.count > 0{
            return _problema.count
        }else{
            return _tipoServicio.count
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if _problema.count > 0{
            delegate?.sendProblem(idproblema : _problema[indexPath.row].valor ,problema: _problema[indexPath.row].descripcion)
        }else{
            delegate?.sendProblem(idproblema : _tipoServicio[indexPath.row].idcodigo ,problema: _tipoServicio[indexPath.row].descripcion)
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellProblemSelected", for: indexPath) as! ProblemSelectedTableViewCell

        if _problema.count > 0{
            cell.lblProblem.text = _problema[indexPath.row].descripcion
        }else{
            cell.lblProblem.text = _tipoServicio[indexPath.row].descripcion
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.00, alpha:1.0)
        cell.selectedBackgroundView = bgColorView
        
        
        return cell
    }
}
