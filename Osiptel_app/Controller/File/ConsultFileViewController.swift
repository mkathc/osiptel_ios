//
//  ConsultFileViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/7/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class ConsultFileViewController: UIViewController,NVActivityIndicatorViewable{

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtEmail.delegate = self
        txtEmail.tag = 0
        
        txtPassword.delegate = self
        txtPassword.tag = 1
        
        txtEmail.text = UserDefaults.standard.getLoginTrasu()
        
        if UserDefaults.standard.getLoginTrasu() == "" {
            txtEmail.text = UserDefaults.standard.getRegisterTrasu()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    //override func viewWillAppear(_ animated: Bool) {
    //    NotificationCenter.default.addObserver(self, selector: #selector(didTakeScreenshot), name: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
    //}
    
    //override func viewDidAppear(_ animated: Bool) {
    //    NotificationCenter.default.addObserver(self, selector: #selector(didTakeScreenshot), name: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
    //}
    
    
    func didTakeScreenshot(){
        print("Screenshot")        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
        txtPassword.text = ""
        txtEmail.layer.borderWidth = 0
        txtPassword.layer.borderWidth = 0
        self.view.backgroundColor = .white
    }
    
   
    @IBAction func btnConsultAction(_ sender: Any) {
        self.view.endEditing(true)
        resignFirstResponder()

        var isValidate = true
        
        if !isValidEmailAddress(emailAddressString: txtEmail.text!){
            isValidate = false
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = UIColor.red.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
        }
        
        if txtEmail.text?.count == 0{
            isValidate = false
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = UIColor.red.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
        }
        
        if txtPassword.text?.count == 0{
            isValidate = false
            txtPassword.borderStyle = UITextBorderStyle.roundedRect
            txtPassword.layer.borderColor = UIColor.red.cgColor
            txtPassword.layer.cornerRadius = 4
            txtPassword.borderColorView = UIColor.red
            txtPassword.layer.borderWidth = 1.0
            txtPassword.text = ""
        }
        
        if isValidate{
            let size = CGSize(width: 308, height: 30)
            startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                , fadeInAnimation: nil)
            attempFetchLogin(email: txtEmail.text!, contraseña: txtPassword.text!)
        }
    }
    
    func attempFetchLogin(email: String, contraseña: String){
        DataService.shared.requestFetchValidarLogin(with: email, with: contraseña){ [weak self] (login, string) in
            if let error = string {
                print("\(error)")
                self!.stopAnimating()
                self!.txtPassword.text = ""
                self!.alertViewController(titulo: "", mensaje: error, closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }
            guard let login = login else{
                return
            }
            print("\(login)")
            self?.updateUI(with: login)
        }
    }
    
    func updateUI(with login: ResponseLogin) {
        stopAnimating()
        UserDefaults.standard.setSesionTime(value: true)
        UserDefaults.standard.setRegisterTrasu(value: txtEmail.text!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MyFileTableViewController") as! MyFileTableViewController
        let backItem = UIBarButtonItem()
        print("Sesion \(UserDefaults.standard.getSesionTime())")
        
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        controller._login = login

        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        self.view.endEditing(true)
        resignFirstResponder()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RegisterTRASUViewController") as! RegisterTRASUViewController
        let backItem = UIBarButtonItem()
        
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 0{
            
            txtEmail.layer.borderWidth = 0
        }
        
        if textField.tag == 1{
            txtPassword.layer.borderWidth = 0
        }
        
        return true
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= 150
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y == -150{
            self.view.frame.origin.y += 150
        }
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        print("Aqui")
        // Pass the selected object to the new view controller.
    }
    

}

extension ConsultFileViewController: UITextFieldDelegate{
    
}
