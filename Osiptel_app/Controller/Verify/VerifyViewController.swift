//
//  VerifyViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/18/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class VerifyViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var OperatorTable: UITableView!
    
    var _operadores = Operador()
    
    var kHeaderSectionTag: Int = 6900;

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1596710384, green: 0.7130240202, blue: 0.8868247867, alpha: 1)

        navigationItem.title = "Verifica tu linea"
        
        let item = UIBarButtonItem(image: UIImage(named: "ic_information_home"),
            style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = item
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)

        attempFetchOperador()
        self.title = "Verifica tu linea"
        
        OperatorTable.delegate = self
        OperatorTable.dataSource = self
        if #available(iOS 11.0, *) {
            OperatorTable.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        OperatorTable.tableFooterView = UIView()
    }
    
    func attempFetchOperador(){
        DataService.shared.requestFetchOperadores{ [weak self] (operador, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let operador = operador else{
                return
            }
            self?.updateOperador(with: operador)
        }
    }
    
    func updateOperador(with operador: Operador) {
        
        _operadores = operador
        OperatorTable.reloadData()
        stopAnimating()
    }
}

extension VerifyViewController: UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red:0.91, green:0.49, blue:0.13, alpha:1.0)
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = .center
        header.roundedCornersAll()
        header.roundedCorners(top: true)
        header.layer.cornerRadius = 20
        
        let headerFrame = self.view.frame.size
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Enlaces a operadores"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _operadores.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "operatorCell", for: indexPath) as! VerifyTableViewCell
        
        let original =  _operadores[indexPath.row].nombreImagen
        let encoded = original!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        cell.imageOperator.sd_setImage(with: URL(string: encoded ?? ""))
        cell.imageOperator.accessibilityLabel = _operadores[indexPath.row].nombreComercial
        
        cell.accessibilityLabel = "Ir a \(_operadores[indexPath.row].nombreComercial)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ExternalWebViewController") as! ExternalWebViewController
        controller.urlExternal = _operadores[indexPath.row].enlace as! String
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)
    }
}
