//
//  ExternalWebViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/18/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import WebKit

class ExternalWebViewController: UIViewController {

    
    @IBOutlet weak var txtWeb: UITextField!
    @IBOutlet weak var externalView: UIView!
    
    @IBOutlet weak var NavigationView: UIView!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBAction func CloseButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    private var webView: WKWebView!
    //let progressView = UIProgressView(progressViewStyle: .default)
    private var estimatedProgressObserver: NSKeyValueObservation?

    var urlExternal: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let webViewPreferences = WKPreferences()
        webViewPreferences.javaScriptEnabled = true
        webViewPreferences.javaScriptCanOpenWindowsAutomatically = true
        
        let webViewConf = WKWebViewConfiguration()
        webViewConf.preferences = webViewPreferences
        webView = WKWebView(frame: externalView.frame, configuration: webViewConf)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.frame = CGRect(origin: CGPoint.zero, size: externalView.frame.size)
        webView.contentMode = .scaleAspectFit

        externalView.addSubview(webView)

        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
        
        load(url: urlExternal)
        txtWeb.text = urlExternal   
    }
    
    private func load(url: String){
        webView.load(URLRequest(url: URL(string: url)!))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            print(self.webView.estimatedProgress)
            self.progressView.progress = Float(self.webView.estimatedProgress)
            print("Progress View \(Float(self.webView.estimatedProgress))")
            
            if Float(self.webView.estimatedProgress) == 1.0{
                self.progressView.alpha = 0
            }
        }
    }
    
}
extension ExternalWebViewController: UIWebViewDelegate, WKNavigationDelegate, WKUIDelegate{
    
 
}
