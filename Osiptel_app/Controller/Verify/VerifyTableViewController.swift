//
//  VerifyTableViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/15/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class VerifyTableViewController: UITableViewController {

    let kHeaderSectionTag: Int = 6900;
    let Opciones = [["id":"1","nombre":"Consulta IMEI","imagen":"ic_consulta_IMEI"],["id":"2","nombre":"Señal OSIPTEL","imagen":"ic_senal_osiptel"],["id":"3","nombre":"Comparatel","imagen":"ic_comparatel"],["id":"4","nombre":"Expedientes Trasu","imagen":"ic_expedientes"],["id":"5","nombre":"Guía de Información","imagen":"ic_guia_informativa"]] as [[String:AnyObject]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        tableView.tableFooterView = UIView()
        
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        return "Enlaces a operadores"
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red:0.82, green:0.40, blue:0.10, alpha:1.0)
        header.textLabel?.textColor = UIColor.white
        
        header.roundedCornersAll()
        //header.frame = UIEdgeInsetsInsetRect(header.frame, UIEdgeInsetsMake(10, 0, 10, 0))
        header.roundedCorners(top: true)
        header.layer.cornerRadius = 20

        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size

    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0;
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Opciones.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "operatorCell", for: indexPath) as! VerifyTableViewCell

        cell.imageOperator.image = UIImage.init(named: "ic_consulta_IMEI")
        
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
