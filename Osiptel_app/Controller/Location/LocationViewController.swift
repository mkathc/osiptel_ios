//
//  LocationViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 1/31/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

private let kPersonWishListAnnotationName = "kPersonWishListAnnotationName"

protocol sendOfficedelegate: class {
    func sendOffice(officeElement: OficinaElement)
}


class LocationViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, MKMapViewDelegate, CLLocationManagerDelegate {
    //func detailsRequestedForPerson(person: Person) {
     //   self.selectedPerson = person
       // self.performSegue(withIdentifier: "personDetails", sender: nil)

    func showDetailPoint() {
        
    }

    @IBOutlet weak var viewLocatorButton: UIView!
    @IBOutlet weak var OfficesMapKit: MKMapView!
    @IBOutlet weak var txtDepartment: UITextField!
    private var DepartmentPicker : UIPickerView?

    var _departamento = Departamento()
    var _oficina = Oficina()
    var _departamentoElement: DepartamentoElement?
    
    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    var count = 0
    var isFirst = true
    var lastAnnotation = MKAnnotationView()
    // data
    var selected : OficinaElement?
    //var selectedPerson: Person?
    
    var locality = ""
    var administrativeArea = ""
    var country = ""
    weak var delegateOffice : sendOfficedelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print("Counttry code \(countryCode)")
        }
        
        let swiftLocale: Locale = Locale.current
        let swiftCountry: String? = swiftLocale.regionCode

        self.DepartmentPicker = UIPickerView()
        self.DepartmentPicker?.delegate = self
        self.DepartmentPicker?.dataSource = self
        txtDepartment.inputView = self.DepartmentPicker
        
        txtDepartment.tag = 0
        attempFetchDepartamentos()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnLocation(_:)))
        viewLocatorButton.addGestureRecognizer(tapGesture)
       
        self.OfficesMapKit.delegate = self;
    }
    
    @objc private final func tapOnLocation(_ tapGesture: UITapGestureRecognizer){
        centerViewOnUserLocation()
    }
    
    func centerViewOnUserLocation(){
        if let location = manager.location?.coordinate{
            let span = MKCoordinateSpan.init(latitudeDelta: 0.0075, longitudeDelta: 0.0075)
            let region = MKCoordinateRegion.init(center: location, span: span)
            OfficesMapKit.setRegion(region, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    func configurePeopleInMap() {
        var annotations = [MKAnnotation]()
      //  for person in PeopleWishListManager.sharedInstance.people {}
        OfficesMapKit.removeAnnotations(OfficesMapKit.annotations)
        OfficesMapKit.addAnnotations(annotations)
    }
    
    

    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    
    
    func attempFetchDepartamentos(){
        DataService.shared.requestFetchAllDepartamento{ [weak self] (departamento, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            
            if(departamento == nil && error == nil){
                DataService.shared.refreshRequestToken(){[weak self] (String, error) in
                    if (String == "Success"){
                        if(self!.isFirst){
                            self!.attempFetchDepartamentos()
                            self!.isFirst = false
                        }
                    }
                }
            }
            
            guard let departamento = departamento else{
                return
            }            
            self?.updateDepartamentos(with: departamento)
        }
    }
    
    func updateDepartamentos(with departamento: Departamento) {
        
        _departamento = departamento
        _departamento.insert(DepartamentoElement.init(codigo: "0", departamento: "Seleccione"), at: 0)
        _departamento.insert(DepartamentoElement.init(codigo: "-1", departamento: "Todos los departamentos"), at: 1)
        DepartmentPicker?.reloadAllComponents()
    }
    
    func attempFetchOficinas(withId id: String, withId nombre: String){
        
        var departamento = ""
        if nombre != "Todos los departamentos"{
            departamento = nombre
        }

        if nombre != "Seleccione"{
        
            DataService.shared.requestFetchOficina(with: id, with: departamento){ [weak self] (oficina, error) in
                
                if let error = error {
                    print("error \(error.localizedDescription)")
                }
                guard let oficina = oficina else{
                    return
                }
                self?.updateOficinas(with: oficina)
            }
        }
    }
    
    func updateOficinas(with oficina: Oficina) {
        
        _oficina = oficina        
        removeAddAnnotationPoint()
        addAnnotationPoint()
    }
    
    
    func removeAddAnnotationPoint(){
        self.OfficesMapKit.annotations.forEach {
            if !($0 is MKUserLocation) {
                self.OfficesMapKit.removeAnnotation($0)
            }
        }
        
        if(lastAnnotation != nil){
            for subview in lastAnnotation.subviews {
                if (subview is DetailMapView) {
                    print(subview)
                    subview.removeFromSuperview()
                }
            }
        }
    }
    
    //Llamando a picker
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.DepartmentPicker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        
        self.DepartmentPicker = UIPickerView()
        self.DepartmentPicker?.delegate = self
        self.DepartmentPicker?.dataSource = self
        self.DepartmentPicker?.backgroundColor = UIColor.white
        txtDepartment.inputView = self.DepartmentPicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }

    //Acciones Picker
    @objc func doneClick() {
        let row:Int = (DepartmentPicker?.selectedRow(inComponent: 0) ?? 0) as Int
        
        attempFetchOficinas(withId: _departamento[row].codigo, withId:_departamento[row].departamento)
        self.txtDepartment.text = _departamento[row].departamento
        txtDepartment.resignFirstResponder()
    }
    @objc func cancelClick() {
        txtDepartment.resignFirstResponder()
    }    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickUp(txtDepartment)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return _departamento.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return _departamento[row].departamento
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if _departamento.count != 0{

        }
    }
    
    func addAnnotationPoint()
    {
        count = _oficina.count - 1
        for data in _oficina
        {
            let point = MKPointAnnotation()
            let pointlatitude = Double(data.latitud)!
            let pointlongitude = Double(data.longitud)!
            point.coordinate = CLLocationCoordinate2DMake(pointlatitude ,pointlongitude)
            let PointMap = MapPin(oficina: data.nombreOficina, direccion: data.direccion ?? "", horarioLV: data.horarioLV  ?? "", horarioS: data.horarioS ?? "", horarioD: data.horarioD ?? "")
            
            PointMap.coordinate = CLLocationCoordinate2DMake(pointlatitude ,pointlongitude)
            self.OfficesMapKit.addAnnotation(PointMap)
            
        }
        var zoom = 4.075
        if txtDepartment.text == "Todos los departamentos"{
            zoom = 18.000
        }
        
        if _oficina.count > 0{
            let span = MKCoordinateSpanMake(zoom, zoom)
            let latitudInicial = Double(_oficina[0].latitud)!
            let longitudInicial = Double(_oficina[0].longitud)!
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitudInicial, longitude: longitudInicial), span: span)
            
            self.OfficesMapKit.setRegion(region, animated: true)
        }
    }
  
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        for childView:AnyObject in view.subviews{
            childView.removeFromSuperview();
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation!.isKind(of: MKUserLocation.self){
            return
        }
        let customView = Bundle.main.loadNibNamed("DetailMapView", owner: self, options: nil)?[0] as! DetailMapView
        let calloutViewFrame = customView.frame;
        customView.frame = CGRect(x: -calloutViewFrame.size.width/2.23, y: -calloutViewFrame.size.height-7, width: 200, height: 160)
        let cpa = view.annotation as! MapPin
        customView.NombreOficina.text = cpa.oficina
        customView.DireccionOficina.text = cpa.direccion
        customView.HorarioLV.text = (cpa.horarioLV != "") ? "L-V: \(cpa.horarioLV!)" : ""
        customView.HorarioSD.text = (cpa.horarioS != "") ? "S: \(cpa.horarioS!)" : ""
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span)
        OfficesMapKit.setRegion(region, animated: true)
        view.addSubview(customView)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       
        let identifier = "CustomAnnotationOficina"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
            annotationView!.isEnabled = true

            annotationView!.image = UIImage(named: "pin_celeste")!
            
        } else {
            annotationView!.annotation = annotation
        }
      
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }

        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            let label1 = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
            label1.text = _oficina[count].nombreOficina
            label1.numberOfLines = 0
            annotationView!.detailCalloutAccessoryView = label1;
            let width = NSLayoutConstraint(item: label1, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.lessThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 200)
            label1.addConstraint(width)
            let height = NSLayoutConstraint(item: label1, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 90)
            label1.addConstraint(height)
            
        } else {
            annotationView!.annotation = annotation
        }
        
        count = count - 1
        
        if #available(iOS 12, *){
           print("iosVersion 12")
        }else{
            let tapGestureAnnotation = MyTapGesture(target: self, action: #selector(tapOnLocationAnnotationView))
            let cpa = annotationView!.annotation as! MapPin
            tapGestureAnnotation.oficina = cpa.oficina!
            tapGestureAnnotation.direccion = cpa.direccion!
            tapGestureAnnotation.horarioLV = (cpa.horarioLV != "") ? "L-V: \(cpa.horarioLV!)" : ""
            tapGestureAnnotation.horarioSD = (cpa.horarioS != "") ? "S: \(cpa.horarioS!)" : ""
            tapGestureAnnotation.annotationViewMap = annotationView!
            annotationView?.addGestureRecognizer(tapGestureAnnotation)
        }
        

        
      /*  if (os.majorVersion  ) {
            // use the feature only available in iOS 9
            // for ex. UIStackView
            let tapGestureAnnotation = MyTapGesture(target: self, action: #selector(tapOnLocationAnnotationView))
            let cpa = annotationView!.annotation as! MapPin
            tapGestureAnnotation.oficina = cpa.oficina!
            tapGestureAnnotation.direccion = cpa.direccion!
            tapGestureAnnotation.horarioLV = (cpa.horarioLV != "") ? "L-V: \(cpa.horarioLV!)" : ""
            tapGestureAnnotation.horarioSD = (cpa.horarioS != "") ? "S: \(cpa.horarioS!)" : ""
            tapGestureAnnotation.annotationViewMap = annotationView!
            annotationView?.addGestureRecognizer(tapGestureAnnotation)
        }*/
     
        return annotationView
    }
    
    @objc func tapOnLocationAnnotationView(sender: MyTapGesture){
        
        if sender.annotationViewMap.isKind(of: MKAnnotationView.self)
        {
          
            for subview in OfficesMapKit.subviews {
                    print(subview)
            }
            
            if(sender.annotationViewMap.subviews.count == 0){
                
                if(lastAnnotation != nil){
                    for subview in lastAnnotation.subviews {
                        if (subview is DetailMapView) {
                            print(subview)
                            subview.removeFromSuperview()
                        }
                    }
                }
                
                let customView = Bundle.main.loadNibNamed("DetailMapView", owner: self, options: nil)?[0] as! DetailMapView
                let calloutViewFrame = customView.frame;
                customView.frame = CGRect(x: -calloutViewFrame.size.width/2.23, y: -calloutViewFrame.size.height-7, width: 200, height: 160)
                customView.NombreOficina.text = sender.oficina
                customView.DireccionOficina.text = sender.direccion
                customView.HorarioLV.text = sender.horarioLV
                customView.HorarioSD.text = sender.horarioSD
                let region = MKCoordinateRegion(center: sender.annotationViewMap.annotation!.coordinate, span: OfficesMapKit.region.span)
                OfficesMapKit.setRegion(region, animated: true)
                sender.annotationViewMap.addSubview(customView)
                lastAnnotation = sender.annotationViewMap
                
            }else{
                for subview in sender.annotationViewMap.subviews {
                    if (subview is DetailMapView) {
                        print(subview)
                        subview.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    //DetailMapView
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        manager.stopUpdatingLocation()
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                self.attempFetchOficinas(withId: "0", withId: placemark.administrativeArea!)
            }
        })
    }
    
    func userLocationString() -> String {
        let userLocationString = "\(locality), \(administrativeArea), \(country)"
        return userLocationString
    }
}

class MyTapGesture: UITapGestureRecognizer {
    var oficina = String()
    var direccion = String()
    var horarioLV = String()
    var horarioSD = String()
    var annotationViewMap = MKAnnotationView()
}

