//
//  ConsultIMEIViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/5/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import EasyTipView

class ConsultIMEIViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var lblIMEINumber: UILabel!
    @IBOutlet weak var txtImei: UITextField!
    @IBOutlet weak var validateImei: UILabel!
    @IBOutlet weak var BlurEffect: UIVisualEffectView!
    @IBOutlet weak var infoButton: UIImageView!
    @IBOutlet weak var ViewTitlePopUpInfo: UIView!
    @IBOutlet weak var ViewTitleNoReportado: UIView!
    @IBOutlet weak var btnReportar: UIButton!
    @IBOutlet weak var txtCodeImei: UILabel!
    
    //-----View-------//
    @IBOutlet var popUpViewInfo: UIView!
    @IBOutlet var ViewNoReportado: UIView!
    
    @IBOutlet var popUpViewDetail: UIView!
    @IBOutlet weak var btnCerrarDetail: UIButton!
    @IBOutlet weak var lblEstadoEquipo: UILabel!
    
    @IBOutlet weak var lblImeiSerie: UILabel!
    @IBOutlet weak var lblEmpresaOperadora: UILabel!
    
    @IBOutlet weak var lblFechaReporte: UILabel!
    @IBOutlet weak var HeaderDetail: UIView!
    var _imei = Imei()
    var contImeitTxt = 0
    var visualEffect:UIVisualEffect!
    let myColor = UIColor.red

    var timer:Timer?
    var timeLeft = 4
    var tipView :EasyTipView?
    var isFirst : Bool = true
    
    override func viewDidAppear(_ animated: Bool) {
        if !UserDefaults.standard.getImeiGuard(){
            txtImei.text = ""
            txtImei.layer.borderWidth = 0
            dismissKeyboard()
            validateImei.alpha = 0
        }else{
            UserDefaults.standard.setImeiGuard(value: false)
        }        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtImei.delegate = self
        txtImei.keyboardType = .numberPad
        BlurEffect.alpha = 0
        
        visualEffect = BlurEffect.effect
        BlurEffect.effect = nil
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1596710384, green: 0.7130240202, blue: 0.8868247867, alpha: 1)
        
        let item = UIBarButtonItem(image: UIImage(named: "ic_information_home"), style: .plain, target: self, action: #selector(ConsultIMEIViewController.addTapped(_:)))
                                
        navigationItem.rightBarButtonItem = item
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissModal(_:)))
        tapGesture.cancelsTouchesInView = true
        BlurEffect.addGestureRecognizer(tapGesture)
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = #colorLiteral(red: 1, green: 0.5329053591, blue: 0.2610576485, alpha: 1)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        
        EasyTipView.globalPreferences = preferences
        
        tipView = EasyTipView(text: "Obtén tu código marcando al número *#06#.", preferences: preferences)
        
        tipView?.show(forView: txtCodeImei)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
   
    }
    
    @objc func onTimerFires()
    {
        timeLeft -= 1
        if timeLeft <= 0 {
            timer?.invalidate()
            timer = nil
            
            if isFirst {
                tipView?.dismiss()
                
                timeLeft = 4
                
                var preferences = EasyTipView.Preferences()
                preferences.drawing.foregroundColor = UIColor.white
                preferences.drawing.backgroundColor = #colorLiteral(red: 1, green: 0.5329053591, blue: 0.2610576485, alpha: 1)
                preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
                
                
                tipView = EasyTipView(text: "Reporta aquí si tu IMEI figura en nuestros datos como bloqueado.", preferences: preferences)
                
                tipView?.show(forView: btnReportar)
                
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
                
                isFirst = false
                
            }else{
                tipView?.dismiss()
            }
            

        }
    }
    
    @objc func addTapped(_ sender: UIBarButtonItem){
        isFirst = true
        timeLeft = 5
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = #colorLiteral(red: 1, green: 0.5329053591, blue: 0.2610576485, alpha: 1)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        
        EasyTipView.globalPreferences = preferences
        
        tipView = EasyTipView(text: "Obtén tu código marcando al número *#06#.", preferences: preferences)
        
        tipView?.show(forView: txtCodeImei)
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTimerFires), userInfo: nil, repeats: true)
    }
    
    func openDetalleReportado(){

        HeaderDetail.roundedCorners(top: true)
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        BlurEffect.alpha = 1
        self.view.addSubview(popUpViewDetail)
        popUpViewDetail.center = BlurEffect.center
        //popUpViewDetail.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        popUpViewDetail.alpha = 0
        
        //UIView.animate(withDuration: 0.4) {
            self.BlurEffect.effect = self.visualEffect
            self.popUpViewDetail.alpha = 1
            self.popUpViewDetail.transform = CGAffineTransform.identity
        //}
    }
    
    @objc func dismissModal(_ sender: UITapGestureRecognizer){
        dismiss()
    }
    
    func dismiss(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.popUpViewInfo.alpha = 0
            self.ViewNoReportado.alpha = 0
            self.popUpViewDetail.alpha = 0
            self.BlurEffect.effect = nil
            self.BlurEffect.alpha = 0
            
        }) { (success:Bool) in
            self.popUpViewInfo.removeFromSuperview()
            self.ViewNoReportado.removeFromSuperview()
            self.popUpViewDetail.removeFromSuperview()
        }
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        guard let url = URL(string: "http://www.osiptel.gob.pe/sistemas/sigem.html") else { return }
        UIApplication.shared.openURL(url)
    }

    @objc
    func tapInfo(sender:UITapGestureRecognizer) {
        
        ViewTitlePopUpInfo.roundedCorners(top: true)

        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        BlurEffect.alpha = 1
        self.view.addSubview(popUpViewInfo)
        popUpViewInfo.center = BlurEffect.center        
        //popUpViewInfo.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        popUpViewInfo.alpha = 0
        
        //UIView.animate(withDuration: 0.4) {
            self.BlurEffect.effect = self.visualEffect
            self.BlurEffect.translatesAutoresizingMaskIntoConstraints = false
            self.popUpViewInfo.alpha = 1
            self.popUpViewInfo.transform = CGAffineTransform.identity
            UIApplication.shared.isStatusBarHidden = true
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.tabBarController?.tabBar.isHidden = true

        //}
    }
    
    func attempFetchImei(withId imei: String, withId uuid: String){
        DataService.shared.requestFetchImei(with: imei, with: uuid){ [weak self] (imei, error) in
            
            self!.stopAnimating()
            
            if error == nil && imei == nil{                
                let generator = UIImpactFeedbackGenerator(style: .heavy)
                generator.impactOccurred()
                
                self!.BlurEffect.alpha = 1
                self!.view.addSubview(self!.ViewNoReportado)
                self!.ViewNoReportado.center = self!.BlurEffect.center
                self!.BlurEffect.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                self!.ViewNoReportado.alpha = 0
                
                //UIView.animate(withDuration: 0.4) {
                    self!.BlurEffect.effect = self!.visualEffect
                    self!.ViewNoReportado.alpha = 1
                    self!.ViewNoReportado.transform = CGAffineTransform.identity
                    self?.lblIMEINumber.text = "El IMEI Serie: \(self!.txtImei.text!)"
                    UIApplication.shared.isStatusBarHidden = true
                    self!.navigationController?.setNavigationBarHidden(true, animated: false)
                    self!.tabBarController?.tabBar.isHidden = true
                //}
            }
            
            if let error = error {
                print("error \(error.localizedDescription)")
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            
            }
            guard let imei = imei else{
                return
            }
            self?.updateImei(with: imei)
        }
    }
    
    func updateImei(with imei: Imei) {
        
        _imei = imei
        
        if _imei.count > 1{
            UserDefaults.standard.setImeiGuard(value: true)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "imeiListTableViewController") as! imeiListTableViewController
            controller._imeiModel = _imei
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            
            HeaderDetail.roundedCorners(top: true)
            
            popUpViewDetail.roundedCornersAll()
            
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            
            let imeiFirst = _imei.first
            
            BlurEffect.alpha = 1
            self.view.addSubview(popUpViewDetail)
            popUpViewDetail.center = BlurEffect.center
            BlurEffect.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            //popUpViewDetail.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            popUpViewDetail.alpha = 0
            
            //UIView.animate(withDuration: 0.4) {
                self.BlurEffect.effect = self.visualEffect
                self.popUpViewDetail.alpha = 1
                self.popUpViewDetail.transform = CGAffineTransform.identity
                //self.lblIMEINumber.text = "El IMEI Serie: \(self.txtImei.text!)"
                self.lblImeiSerie.text = imeiFirst?.ne
                self.lblEstadoEquipo.text = imeiFirst?.reporte
                self.lblEmpresaOperadora.text = imeiFirst?.operador
                self.lblFechaReporte.text = imeiFirst?.fechaHora
                UIApplication.shared.isStatusBarHidden = true   
                self.navigationController?.setNavigationBarHidden(true, animated: false)
                self.tabBarController?.tabBar.isHidden = true
            //}
        }
        
    }
    
    
    @IBAction func btnConsultarImei(_ sender: Any) {
        
        contImeitTxt =  txtImei.text?.count ?? 0
        validateImei.alpha = 1       
        txtImei.borderStyle = UITextBorderStyle.roundedRect
        txtImei.layer.borderColor = myColor.cgColor
        txtImei.layer.cornerRadius = 4
        
        
        if contImeitTxt == 0{
            txtImei.borderColorView = UIColor.red
            validateImei.text = "Se debe ingresar 15 caracteres"
            txtImei.layer.borderWidth = 1.0

        }else if contImeitTxt < 15{
            txtImei.borderColorView = UIColor.red
            validateImei.text = "Se debe ingresar 15 caracteres"
            txtImei.layer.borderWidth = 1.0
        }else if contImeitTxt == 15{
            txtImei.layer.borderWidth = 0
            dismissKeyboard()
            validateImei.alpha = 0
            ViewTitleNoReportado.roundedCorners(top: true)
            
            let size = CGSize(width: 308, height: 30)
            startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                , fadeInAnimation: nil)
            
            attempFetchImei(withId: txtImei.text!, withId: UserDefaults.standard.getUUID())
        }
    }
    
    @IBAction func btnReportAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ReportProblemViewController") as! ReportProblemViewController
        controller.imei = txtImei.text!        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func closePopUpInfo(_ sender: Any) {
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()

        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
        UIView.animate(withDuration: 0.3, animations: {

            self.popUpViewInfo.alpha = 0
            self.ViewNoReportado.alpha = 0
            self.popUpViewDetail.alpha = 0
            self.BlurEffect.effect = nil
            self.BlurEffect.alpha = 0

        }) { (success:Bool) in
            self.popUpViewInfo.removeFromSuperview()
            self.ViewNoReportado.removeFromSuperview()
            self.popUpViewDetail.removeFromSuperview()

        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        txtImei.borderColorView = nil
        txtImei.borderWidthView = 0
        validateImei.alpha = 0
        
        if textField.tag == 0{
            let count: Int = textField.text?.count ?? 0
            
            if count > 14{
                validateImei.alpha = 0
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace: Int = Int(strcmp(char, "\u{8}"))
                if isBackSpace != -8 {
                    dismissKeyboard()
                }
            }
        }
        
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 15
    }
}

