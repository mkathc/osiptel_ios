

import UIKit


class ImeiListViewController: UIViewController{
    
    let kHeaderSectionTag: Int = 6900;
    var _imeiModel = Imei()
    var _imeiElement : ImeiElement!
    
    let sectionSelected = 0
    
    @IBOutlet weak var TableFiles: UITableView!
    
    var expandedSectionHeaderNumber: Int = 0
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    var sectionItems2: Array<Any> = []
    var sectionNames2: Array<Any> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Consulta IMEI"
        
        TableFiles.delegate = self
        TableFiles.dataSource = self
        
        for modelo in _imeiModel{
            sectionNames.append(modelo.ne)
        }
        
        for datosModelo in _imeiModel{
            sectionItems.append([datosModelo])
        }
         
        if #available(iOS 11.0, *) {
            self.TableFiles.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.TableFiles!.tableFooterView = UIView()
        self.TableFiles.frame = UIEdgeInsetsInsetRect(self.TableFiles.frame, UIEdgeInsetsMake(10, 16, 10, 16))
        
    }
}

extension ImeiListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "No se ecuentraron expedientes \nascoiados a su cuenta."
            messageLabel.numberOfLines = 0;
            messageLabel.textColor = UIColor.black
            messageLabel.textAlignment = .center;
            //messageLabel.font = UIFont(name: "MuseoSans", size: 20.0)!
            messageLabel.sizeToFit()
            self.TableFiles.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return "Imei \(self.sectionNames[section] as! String)"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 10;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        footerView.backgroundColor = UIColor.white
        footerView.borderColorView = UIColor.white
        return footerView
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
        header.textLabel?.textColor = UIColor.white
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        
        var theImageView : UIImageView = UIImageView()
        
        if section == 0{
            theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 70, y: 25, width: 18, height: 4));
            theImageView.image = UIImage(named: "ic__")
        }else{
            theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 70, y: 13, width: 18, height: 18));
            theImageView.image = UIImage(named: "ic_+")
        }

        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        header.roundedCornersAll()

        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
        
        self.TableFiles.headerView(forSection: 0)?.roundedCorners(top: true)
        self.TableFiles.headerView(forSection: 0)?.contentView.backgroundColor = UIColor(red:0.99, green:0.74, blue:0.11, alpha:1.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellImeiList", for: indexPath) as! ListImeiTableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.textColor = UIColor.black
        _imeiElement = section[indexPath.row] as! ImeiElement
        cell.lblFecha.text = _imeiElement.fechaHora
        cell.lblOperadora.text = _imeiElement.operador
        cell.lblEstadoEquipo.text = _imeiElement.reporte
        //cell.roundedCorners(top: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        for i in 0 ..< sectionNames.count{
            TableFiles.headerView(forSection: i)?.roundedCornersAll()
            
            TableFiles.headerView(forSection: i)?.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
        }
        
        //headerView.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
            headerView.roundedCorners(top: true)
            headerView.contentView.backgroundColor = UIColor(red:0.99, green:0.74, blue:0.11, alpha:1.0)
            
        } else {
            
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!, headerView: headerView)
                headerView.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
                //headerView.roundedCornersAll()
            } else {
                headerView.contentView.backgroundColor = UIColor(red:0.99, green:0.74, blue:0.11, alpha:1.0)
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!, headerView: headerView)
                tableViewExpandSection(section, imageView: eImageView!)
                headerView.roundedCorners(top: true)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView, headerView: UIView) {
        let sectionData = self.sectionItems[section] as! NSArray
        self.expandedSectionHeaderNumber = -1;

        if (sectionData.count == 0) {
            return;
        } else {
            
            UIView.animate(withDuration: 0.4, animations: {
                let headerFrame = self.view.frame.size
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
                imageView.image = UIImage(named: "ic_+")
                imageView.frame = CGRect(x: headerFrame.width - 70, y: 13, width: 18, height: 18)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.TableFiles!.beginUpdates()
            self.TableFiles!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            headerView.roundedCornersAll()
            
            self.TableFiles!.endUpdates()
            
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                let headerFrame = self.view.frame.size
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
                imageView.image = UIImage(named: "ic__")
                imageView.frame = CGRect(x: headerFrame.width - 70, y: 25, width: 18, height: 4)
            })
            var indexesPath = [IndexPath]()
            
           
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i , section: section)
                indexesPath.append(index)
            }
            
            self.expandedSectionHeaderNumber = section
            self.TableFiles!.beginUpdates()
            self.TableFiles!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            //self.TableFiles.roundedCorners(top: false)
            
            self.TableFiles.tableHeaderView?.roundedCornersAll()
            self.TableFiles!.endUpdates()
        }
    }
    
    
}
