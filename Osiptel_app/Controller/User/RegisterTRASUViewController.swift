//
//  RegisterUserViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 2/26/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RegisterTRASUViewController: UIViewController,NVActivityIndicatorViewable,selectedProblemDelegate {
    
    func sendProblem(idproblema: String, problema: String) {
        txtServicio.text = problema
        idTipoServicio = Int(idproblema)!
    }
    
    @IBOutlet weak var txtTipoDocumento: UITextField!
    @IBOutlet weak var txtNumeroDocumento: UITextField!
    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtApellidoP: UITextField!
    @IBOutlet weak var txtApellidoM: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtReEmail: UITextField!
    @IBOutlet weak var txtContraseña: UITextField!
    @IBOutlet weak var txtReContraseña: UITextField!
    @IBOutlet weak var scrollReport: UIScrollView!
    private var PickerTipoDocumento : UIPickerView?
    @IBOutlet weak var txtServicio: UITextField!
    public var PickerServicio : UIPickerView?
    @IBOutlet weak var viewContainerTrasu: UIView!
    @IBOutlet weak var EtiquetaNombre: UILabel!
    @IBOutlet weak var lblValidateEmail: UILabel!
    @IBOutlet weak var lblValidatePassword: UILabel!
    private var PickerCodigoAsociado : UIPickerView?
    @IBOutlet weak var txtCodigoAsociado: UITextField!
    @IBOutlet weak var txtCodigoReclamo: UITextField!

    var CodigoAsociado = 0
    var idCodAsociado = 0
    var idTipoServicio = 0
    var _tipoDocumento = TipoDocumento()
    var _codAsociado  : [String] = ["Seleccionar","Nro. de teléfono", "Código de reclamo", "Código de cliente"]
    var _tipoServicio = Servicio()
    
    let IP = "1"
    let numeroCelular = UserDefaults.standard.getNumber()
    let MAC = UserDefaults.standard.getUUID()
    let marcaCelular = "Apple"
    let sistemaOperativo = "iOS"
    let modeloCelular = UserDefaults.standard.getModelDevice()
    let nombreUsuario = UserDefaults.standard.getNameComplete()
    var problemSelected = ProblemSelectedTableViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)        
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        problemSelected.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        //NoticacionCenter Movimiento de vista con respecto al teclado
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        txtTipoDocumento.delegate = self
        txtTipoDocumento.text = "Seleccione"
        txtNumeroDocumento.delegate = self
        txtNumeroDocumento.tag = 1
        txtNombres.delegate = self
        txtNombres.tag = 2
        txtApellidoP.delegate = self
        txtApellidoP.tag = 3
        txtApellidoM.delegate = self
        txtApellidoM.tag = 4
        txtEmail.delegate = self
        txtEmail.tag = 5
        txtReEmail.delegate = self
        txtReEmail.tag = 6
        txtCodigoAsociado.delegate = self
        txtCodigoAsociado.text = "Seleccione"
        PickerCodigoAsociado = UIPickerView()
        PickerCodigoAsociado?.delegate = self
        PickerCodigoAsociado?.dataSource = self
        PickerCodigoAsociado?.tag = 20
        txtCodigoAsociado.inputView = PickerCodigoAsociado
        txtCodigoReclamo.delegate = self
        txtCodigoReclamo.tag = 7
        txtContraseña.delegate = self
        txtContraseña.tag = 8
        txtReContraseña.delegate? = self
        txtReContraseña.tag = 100
        PickerTipoDocumento = UIPickerView()
        PickerTipoDocumento?.delegate = self
        PickerTipoDocumento?.dataSource = self
        PickerTipoDocumento?.tag = 0
        txtTipoDocumento.inputView = PickerTipoDocumento
        PickerServicio = UIPickerView()
        PickerServicio?.delegate = self
        PickerServicio?.dataSource = self
        PickerServicio?.tag = 10
        txtServicio.inputView = PickerServicio
        txtServicio.text = "Seleccione"
        lblValidateEmail.alpha = 0
        lblValidatePassword.alpha = 0
        txtNombres.text = UserDefaults.standard.getName()
        txtApellidoP.text = UserDefaults.standard.getLastnameP()
        txtApellidoM.text = UserDefaults.standard.getLastnameM()
        txtEmail.text = UserDefaults.standard.getEmail()
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        
        view.addGestureRecognizer(tapGesture)
        
        //let tapGestureRe = UITapGestureRecognizer(target: self, action: #selector(tapOnTextRe(_:)))
        //txtReContraseña.addGestureRecognizer(tapGestureRe)
        
        let tapGesturetex = UITapGestureRecognizer(target: self, action: #selector(tapOnTextView(_:)))
        txtServicio.addGestureRecognizer(tapGesturetex)
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        attempFetchTipoDocumento()
        attempFetchTipoServicio()
        updateCodigoAsociado()
    }
    
    @objc private final func tapOnTextRe(_ tapGesture: UITapGestureRecognizer){
        txtReContraseña.layer.borderWidth = 0
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }

    @objc private final func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
        txtServicio.layer.borderWidth = 0
        self.view.endEditing(true)
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ProblemSelectedTableViewController") as! ProblemSelectedTableViewController
        let backItem = UIBarButtonItem()
        vc.delegate = self
        backItem.title = " "
        vc._tipoServicio = _tipoServicio
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
    
        var isValidate = true
        var tipoDocumento = ""
        
        if txtNumeroDocumento.text?.count == 0{
            txtNumeroDocumento.borderStyle = UITextBorderStyle.roundedRect
            txtNumeroDocumento.layer.borderColor = UIColor.red.cgColor
            txtNumeroDocumento.layer.cornerRadius = 4
            txtNumeroDocumento.borderColorView = UIColor.red
            txtNumeroDocumento.layer.borderWidth = 1.0
            isValidate = false
        }
        if txtTipoDocumento.text! == "Seleccione"{
            txtTipoDocumento.borderStyle = UITextBorderStyle.roundedRect
            txtTipoDocumento.layer.borderColor = UIColor.red.cgColor
            txtTipoDocumento.layer.cornerRadius = 4
            txtTipoDocumento.borderColorView = UIColor.red
            txtTipoDocumento.layer.borderWidth = 1.0
            isValidate = false
            
            if txtApellidoP.text?.count == 0{
                txtApellidoP.borderStyle = UITextBorderStyle.roundedRect
                txtApellidoP.layer.borderColor = UIColor.red.cgColor
                txtApellidoP.layer.cornerRadius = 4
                txtApellidoP.borderColorView = UIColor.red
                txtApellidoP.layer.borderWidth = 1.0                
            }
            if txtApellidoM.text?.count == 0{
                txtApellidoM.borderStyle = UITextBorderStyle.roundedRect
                txtApellidoM.layer.borderColor = UIColor.red.cgColor
                txtApellidoM.layer.cornerRadius = 4
                txtApellidoM.borderColorView = UIColor.red
                txtApellidoM.layer.borderWidth = 1.0
            }
        }
        
        if txtCodigoAsociado.text! == "Seleccione"{
            txtCodigoAsociado.borderStyle = UITextBorderStyle.roundedRect
            txtCodigoAsociado.layer.borderColor = UIColor.red.cgColor
            txtCodigoAsociado.layer.cornerRadius = 4
            txtCodigoAsociado.borderColorView = UIColor.red
            txtCodigoAsociado.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if txtCodigoAsociado.text! == "Seleccionar"{
            txtCodigoAsociado.borderStyle = UITextBorderStyle.roundedRect
            txtCodigoAsociado.layer.borderColor = UIColor.red.cgColor
            txtCodigoAsociado.layer.cornerRadius = 4
            txtCodigoAsociado.borderColorView = UIColor.red
            txtCodigoAsociado.layer.borderWidth = 1.0
            isValidate = false
        }
        
        
        if txtTipoDocumento.text! == "DNI"{
            if (txtNumeroDocumento.text?.count)! < 8{
                txtNumeroDocumento.borderStyle = UITextBorderStyle.roundedRect
                txtNumeroDocumento.layer.borderColor = UIColor.red.cgColor
                txtNumeroDocumento.layer.cornerRadius = 4
                txtNumeroDocumento.borderColorView = UIColor.red
                txtNumeroDocumento.layer.borderWidth = 1.0
                isValidate = false
            }
            if txtApellidoP.text?.count == 0{
                txtApellidoP.borderStyle = UITextBorderStyle.roundedRect
                txtApellidoP.layer.borderColor = UIColor.red.cgColor
                txtApellidoP.layer.cornerRadius = 4
                txtApellidoP.borderColorView = UIColor.red
                txtApellidoP.layer.borderWidth = 1.0
                isValidate = false
            }
            if txtApellidoM.text?.count == 0{
                txtApellidoM.borderStyle = UITextBorderStyle.roundedRect
                txtApellidoM.layer.borderColor = UIColor.red.cgColor
                txtApellidoM.layer.cornerRadius = 4
                txtApellidoM.borderColorView = UIColor.red
                txtApellidoM.layer.borderWidth = 1.0
                isValidate = false
            }
        }else if txtTipoDocumento.text! == "RUC"{
            if (txtNumeroDocumento.text?.count)! < 11{
                txtNumeroDocumento.borderStyle = UITextBorderStyle.roundedRect
                txtNumeroDocumento.layer.borderColor = UIColor.red.cgColor
                txtNumeroDocumento.layer.cornerRadius = 4
                txtNumeroDocumento.borderColorView = UIColor.red
                txtNumeroDocumento.layer.borderWidth = 1.0
                isValidate = false
            }
        }
        
        if txtNombres.text?.count == 0{
            txtNombres.borderStyle = UITextBorderStyle.roundedRect
            txtNombres.layer.borderColor = UIColor.red.cgColor
            txtNombres.layer.cornerRadius = 4
            txtNombres.borderColorView = UIColor.red
            txtNombres.layer.borderWidth = 1.0
            isValidate = false
        }

        
        if txtEmail.text == ""{
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = UIColor.red.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
            isValidate = false
        }

        if txtReEmail.text == ""{
            txtReEmail.borderStyle = UITextBorderStyle.roundedRect
            txtReEmail.layer.borderColor = UIColor.red.cgColor
            txtReEmail.layer.cornerRadius = 4
            txtReEmail.borderColorView = UIColor.red
            txtReEmail.layer.borderWidth = 1.0
            isValidate = true
        }
        
        if !isValidEmailAddress(emailAddressString: txtEmail.text!){
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = UIColor.red.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if !isValidEmailAddress(emailAddressString: txtReEmail.text!){
            txtReEmail.borderStyle = UITextBorderStyle.roundedRect
            txtReEmail.layer.borderColor = UIColor.red.cgColor
            txtReEmail.layer.cornerRadius = 4
            txtReEmail.borderColorView = UIColor.red
            txtReEmail.layer.borderWidth = 1.0
            isValidate = false
        }

        if txtEmail.text! != txtReEmail.text!{
            txtReEmail.borderStyle = UITextBorderStyle.roundedRect
            txtReEmail.layer.borderColor = UIColor.red.cgColor
            txtReEmail.layer.cornerRadius = 4
            txtReEmail.borderColorView = UIColor.red
            txtReEmail.layer.borderWidth = 1.0
            txtEmail.borderStyle = UITextBorderStyle.roundedRect
            txtEmail.layer.borderColor = UIColor.red.cgColor
            txtEmail.layer.cornerRadius = 4
            txtEmail.borderColorView = UIColor.red
            txtEmail.layer.borderWidth = 1.0
            lblValidateEmail.alpha = 1
            isValidate = false
        }
        
        if txtCodigoReclamo.text!.count == 0{
            txtCodigoReclamo.borderStyle = UITextBorderStyle.roundedRect
            txtCodigoReclamo.layer.borderColor = UIColor.red.cgColor
            txtCodigoReclamo.layer.cornerRadius = 4
            txtCodigoReclamo.borderColorView = UIColor.red
            txtCodigoReclamo.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if txtContraseña.text == ""{
            txtContraseña.borderStyle = UITextBorderStyle.roundedRect
            txtContraseña.layer.borderColor = UIColor.red.cgColor
            txtContraseña.layer.cornerRadius = 4
            txtContraseña.borderColorView = UIColor.red
            txtContraseña.layer.borderWidth = 1.0
            isValidate = false
        }

        if txtReContraseña.text == ""{
            txtReContraseña.borderStyle = UITextBorderStyle.roundedRect
            txtReContraseña.layer.borderColor = UIColor.red.cgColor
            txtReContraseña.layer.cornerRadius = 4
            txtReContraseña.borderColorView = UIColor.red
            txtReContraseña.layer.borderWidth = 1.0
            isValidate = false
        }
        if txtContraseña.text! != txtReContraseña.text!{
            txtReContraseña.borderStyle = UITextBorderStyle.roundedRect
            txtReContraseña.layer.borderColor = UIColor.red.cgColor
            txtReContraseña.layer.cornerRadius = 4
            txtReContraseña.borderColorView = UIColor.red
            txtReContraseña.layer.borderWidth = 1.0
            txtContraseña.borderStyle = UITextBorderStyle.roundedRect
            txtContraseña.layer.borderColor = UIColor.red.cgColor
            txtContraseña.layer.cornerRadius = 4
            txtContraseña.borderColorView = UIColor.red
            txtContraseña.layer.borderWidth = 1.0
            lblValidatePassword.alpha = 1
            isValidate = false
        }
        
        if txtServicio.text! == "" || txtServicio.text! == "Seleccione" {
            txtServicio.borderStyle = UITextBorderStyle.roundedRect
            txtServicio.layer.borderColor = UIColor.red.cgColor
            txtServicio.layer.cornerRadius = 4
            txtServicio.borderColorView = UIColor.red
            txtServicio.layer.borderWidth = 1.0
            isValidate = false
        }
        
        if isValidate{
            if txtTipoDocumento.text! == "DNI"{
                tipoDocumento = "D"
            }else if txtTipoDocumento.text! == "RUC"{
                tipoDocumento = "R"
                txtApellidoP.text = txtNombres.text
                txtApellidoM.text = txtNombres.text
            }
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor: UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                , fadeInAnimation: nil)
            print("codigo asociado \(idCodAsociado)")
            print("codigo reclamo \(txtCodigoReclamo.text!)")

            let postTrasu = PostCuenta.init(tipoDocumento: tipoDocumento, numeroDocumento: txtNumeroDocumento.text!, nombres: txtNombres.text!, apellidoPaterno: txtApellidoP.text!, apellidoMaterno: txtApellidoM.text!, correoElectronico: txtEmail.text!, confirmarCorreoElectronico: txtReEmail.text!, idServicioReclamado: idTipoServicio, idCodigoAsociado: idCodAsociado, codigoReclamo: txtCodigoReclamo.text!, contrasenia: txtContraseña.text!, confirmarContrasenia: txtReContraseña.text!, sistemaOperativo: sistemaOperativo, marcaCelular: marcaCelular, modeloCelular: modeloCelular, numeroCelular: numeroCelular, mac: MAC, nombreUsuario: nombreUsuario)
            
            attempFetchPostRegister(withId: postTrasu)
        }
    }
    
    func attempFetchPostRegister(withId post: PostCuenta){
        
        DataService.shared.requestFetchCreatePostTrasu(with: post) {[weak self] (trasu, error, String) in
            self!.stopAnimating()
            if error != nil {
                print("error")
                print(error)
                //AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            
            if trasu == "Ok"{                
                UserDefaults.standard.setRegisterTrasu(value: self!.txtEmail.text!)
                self!.alertViewController(titulo: "", mensaje: "Su registro fue exitoso, por favor active su cuenta mediante el correo electrónico remitido", closeFlag: true, imageValidate: true, closeAutomatic: false, sendVC: "")
            }else if trasu == "error"{
                self!.alertViewController(titulo: "", mensaje: String!, closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
            }
            

        }
    }

    func attempFetchTipoDocumento(){
        DataService.shared.requestFetchListarTipoDocumento{ [weak self] (tipoDocumento, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let tipoDocumento = tipoDocumento else{
                return
            }
            self?.updateTipoDocumento(with: tipoDocumento)
        }
    }
    
    func updateTipoDocumento(with tipoDocumento: TipoDocumento) {
        _tipoDocumento = tipoDocumento
        _tipoDocumento.insert(TipoDocumentoElement.init(idcodigo:"0",descripcion:"Seleccionar"), at: 0)
        PickerTipoDocumento?.reloadAllComponents()
        stopAnimating()
    }
    
    func updateCodigoAsociado() {
        PickerCodigoAsociado?.reloadAllComponents()
        stopAnimating()
    }
    
    func attempFetchTipoServicio(){
        DataService.shared.requestFetchListarServicio{ [weak self] (servicio, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let servicio = servicio else{
                return
            }
            self?.updateTipoServicio(with: servicio)
        }
    }
    
    func updateTipoServicio(with servicio: Servicio) {

        _tipoServicio = servicio
        
        stopAnimating()
    }
}

extension RegisterTRASUViewController: UIScrollViewDelegate{
    
    @objc func keyboardWillShow(notification:NSNotification){

        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollReport.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollReport.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollReport.contentInset = contentInset
    }
}

extension RegisterTRASUViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if  pickerView.tag == 0{
            return _tipoDocumento.count
        }
        if  pickerView.tag == 10{
            return _tipoServicio.count
        }
        if  pickerView.tag == 20{
            return _codAsociado.count
        }

        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0{
            txtTipoDocumento.text =  _tipoDocumento[row].descripcion
            txtNumeroDocumento.text = ""
            
            if txtTipoDocumento.text == "RUC"{
                EtiquetaNombre.text = "Razón Social"
                txtTipoDocumento.layer.borderWidth = 0
                txtNumeroDocumento.layer.borderWidth = 0
                viewContainerTrasu.frame.origin.y = 240
                txtNombres.layer.borderWidth = 0
            }else if txtTipoDocumento.text == "DNI"{
                EtiquetaNombre.text = "Nombres"
                txtTipoDocumento.layer.borderWidth = 0
                viewContainerTrasu.frame.origin.y = 380
                txtNumeroDocumento.layer.borderWidth = 0
                txtNombres.layer.borderWidth = 0
                txtApellidoP.layer.borderWidth = 0
                txtApellidoM.layer.borderWidth = 0
            }
            
        }
        
        if pickerView.tag == 10{
            txtServicio.layer.borderWidth = 0
        }
        
        if pickerView.tag == 20{
            txtCodigoAsociado.text =  _codAsociado[row]
            txtCodigoAsociado.layer.borderWidth = 0
            txtCodigoReclamo.text =  ""
            
            if(txtCodigoAsociado.text == "Nro. de teléfono"){
                idCodAsociado = 1
                print("entra nro telefono")

            }else{
                if(txtCodigoAsociado.text == "Código de reclamo"){
                    idCodAsociado = 2
                    print("entra codigo reclamo")

                }else{
                    if(txtCodigoAsociado.text == "Código de cliente"){
                        idCodAsociado = 3
                        print("entra codigo cliente")
                    }
                }
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if  pickerView.tag == 0{
            return _tipoDocumento[row].descripcion
        }
        
        if  pickerView.tag == 20{
            return _codAsociado[row]
        }
        return ""
    }
}

extension RegisterTRASUViewController: UITextFieldDelegate{
    
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        if textField.tag == 1{
             var maxLength = 11
            if txtTipoDocumento.text == "RUC"{
                maxLength = 11
                txtNumeroDocumento.layer.borderWidth = 0
            }else if txtTipoDocumento.text == "DNI"{
                maxLength = 8
                txtNumeroDocumento.layer.borderWidth = 0
            }
            
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField.tag == 1{
            txtNumeroDocumento.layer.borderWidth = 0
        }
        if textField.tag == 2{
            txtNombres.layer.borderWidth = 0
        }
        if textField.tag == 3{
            txtApellidoP.layer.borderWidth = 0
        }
        if textField.tag == 4{
            txtApellidoM.layer.borderWidth = 0
        }
        if textField.tag == 5{
            txtEmail.layer.borderWidth = 0
            txtReEmail.layer.borderWidth = 0
            lblValidateEmail.alpha = 0
        }
        if textField.tag == 6{
            txtEmail.layer.borderWidth = 0
            txtReEmail.layer.borderWidth = 0
            lblValidateEmail.alpha = 0
        }
        if textField.tag == 7{
            txtCodigoReclamo.layer.borderWidth = 0
        }
        if textField.tag == 8{
            txtContraseña.layer.borderWidth = 0
            txtReContraseña.layer.borderWidth = 0
            lblValidatePassword.alpha = 0
        }
        if textField.tag == 9{
            txtReContraseña.layer.borderWidth = 0
            txtContraseña.layer.borderWidth = 0
            lblValidatePassword.alpha = 0
        }
        if textField.tag == 100{
            txtReContraseña.layer.borderWidth = 0
            txtContraseña.layer.borderWidth = 0
            lblValidatePassword.alpha = 0
        }
        
        if textField.tag == 2 || textField.tag == 3 || textField.tag == 4{
            
            let allowedCharacters = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            return alphabet
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
