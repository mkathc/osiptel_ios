//
//  ReportAllTableViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/25/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import AEAccordion

class ReportAllTableViewController: AccordionTableViewController, reportAllDelegate {
    
    
    var expediente : Array<String> = []
    
    var _expediente = Expediente()
    var _expedienteItem : ExpedienteElement!
    //Reporte Cobertura
    var _reporte = ReporteCobertura()
    var _reporteItem : ReporteCoberturaElement!
    var _login : ResponseLogin!
    
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        //backButton.setTitle("Back", for: .normal)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.setTitleColor(.white, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(self.backButtonPressed), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barTintColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Reportes"
        addBackButton()
        
        for modelo in _reporte{
            expediente.append(modelo.numEncuesta!)
        }

        registerCell()
        expandFirstCell()
    }

    func registerCell() {
        let cellNib = UINib(nibName: ReportAllTableViewCell.reuseIdentifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: ReportAllTableViewCell.reuseIdentifier)
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = IndexPath(row: 0, section: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    
    // MARK: - Table view data source

    func viewComentary(reporte: String) {
        ModalcomentaryViewController(comentario: reporte)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if expediente.count > 0 {
            return expediente.count
        }else {

            let backgroundImage = UIImage(named: "osiptel_no")
            let imageView = UIImageView(image: backgroundImage)
            imageView.contentMode = .scaleAspectFit
            imageView.layer.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height)
            tableView.backgroundView = imageView;
            
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReportAllTableViewCell.reuseIdentifier, for: indexPath)
        if let cell = cell as? ReportAllTableViewCell {
            cell.day.text = "REPORTE Nº \(expediente[indexPath.row])"
            cell.setReporte(reporte: _reporte[indexPath.row])
            cell.delegate = self
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return expandedIndexPaths.contains(indexPath) ? 300.0 : 70.0
    }

}
