//
//  ReportAllViewViewController.swift
//  Osiptel_app
//
//  Created by GYS on 3/19/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit

class ReportAllViewViewController: UIViewController {

    let kHeaderSectionTag: Int = 6900;
   
    @IBOutlet weak var tblReport: UITableView!
    
    var expandedSectionHeaderNumber: Int = 0
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    var sectionItems2: Array<Any> = []
    var sectionNames2: Array<Any> = []
    
    var _expediente = Expediente()
    var _expedienteItem : ExpedienteElement!
    //Reporte Cobertura
    var _reporte = ReporteCobertura()
    var _reporteItem : ReporteCoberturaElement!
    var _login : ResponseLogin!
    
    func addBackButton() {
        let backButton = UIButton(type: .custom)
        //backButton.setTitle("Back", for: .normal)
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        backButton.setTitleColor(.white, for: .normal) // You can change the TitleColor
        backButton.addTarget(self, action: #selector(self.backButtonPressed), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func backButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barTintColor = UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
        navigationController?.navigationBar.tintColor = .white
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Reportes"
        
        tblReport.delegate = self
        tblReport.dataSource = self
        
        addBackButton()
        
        for modelo in _reporte{
            sectionNames.append(modelo.numEncuesta)
        }
        
        for datosModelo in _reporte{
            sectionItems.append([datosModelo])
        }
        
        if #available(iOS 11.0, *) {
            self.tblReport.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
 
        
        self.tblReport.layer.masksToBounds = true
        self.tblReport.separatorInset = .zero
        self.tblReport.translatesAutoresizingMaskIntoConstraints = false
        
        self.tblReport!.tableFooterView = UIView()
        self.tblReport.frame = UIEdgeInsetsInsetRect(self.tblReport.frame, UIEdgeInsetsMake(10, 16, 10, 16))
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ReportAllViewViewController: UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "No se econtraron reportes."
            messageLabel.numberOfLines = 0;
            messageLabel.textColor = UIColor.black
            messageLabel.textAlignment = .center;
            //messageLabel.font = UIFont(name: "MuseoSans_300", size: 20.0)!
            messageLabel.sizeToFit()
            self.tblReport.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return "Nº Reporte \(self.sectionNames[section] as! String)"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 10;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        footerView.backgroundColor = UIColor.white
        footerView.borderColorView = UIColor.white
        return footerView
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
        header.textLabel?.textColor = UIColor.white
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        
        var theImageView : UIImageView = UIImageView()
        
        if section == 0{
            theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 70, y: 25, width: 18, height: 4));
            theImageView.image = UIImage(named: "ic__")
        }else{
            theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 70, y: 13, width: 18, height: 18));
            theImageView.image = UIImage(named: "ic_+")
        }
        
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        header.roundedCornersAll()

        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
        
        self.tblReport.headerView(forSection: 0)?.roundedCorners(top: true)
        self.tblReport.headerView(forSection: 0)?.contentView.backgroundColor = UIColor(red:0.99, green:0.74, blue:0.11, alpha:1.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReportList", for: indexPath) as! ReportAllTableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.textColor = UIColor.black        
        cell.clipsToBounds = true
        
        _reporteItem = section[indexPath.row] as! ReporteCoberturaElement
        cell.setReporte(reporte: _reporteItem)
        //cell.roundedCorners(top: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        for i in 0 ..< sectionNames.count{
            tblReport.headerView(forSection: i)?.roundedCornersAll()
            
            tblReport.headerView(forSection: i)?.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
        }
        
        //headerView.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
            headerView.roundedCorners(top: true)
            headerView.contentView.backgroundColor = UIColor(red:0.99, green:0.74, blue:0.11, alpha:1.0)
            
        } else {
            
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!, headerView: headerView)
                headerView.contentView.backgroundColor = UIColor(red:0.00, green:0.69, blue:0.85, alpha:1.0)
                //headerView.roundedCornersAll()
            } else {
                headerView.contentView.backgroundColor = UIColor(red:0.99, green:0.74, blue:0.11, alpha:1.0)
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!, headerView: headerView)
                tableViewExpandSection(section, imageView: eImageView!)
                headerView.roundedCorners(top: true)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView, headerView: UIView) {
        let sectionData = self.sectionItems[section] as! NSArray
        self.expandedSectionHeaderNumber = -1;
        
        if (sectionData.count == 0) {
            return;
        } else {
            
            
            UIView.animate(withDuration: 0.4, animations: {
                let headerFrame = self.view.frame.size
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
                imageView.image = UIImage(named: "ic_+")
                imageView.frame = CGRect(x: headerFrame.width - 70, y: 13, width: 18, height: 18)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tblReport!.beginUpdates()
            self.tblReport!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            headerView.roundedCornersAll()
            
            self.tblReport!.endUpdates()
            
        
            let lastScrollOffset = tblReport.contentOffset
            
            tblReport.setContentOffset(lastScrollOffset, animated: false)
            
            self.tblReport.reloadData()
            
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            
            UIView.animate(withDuration: 0.4, animations: {
                let headerFrame = self.view.frame.size
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
                imageView.image = UIImage(named: "ic__")
                imageView.frame = CGRect(x: headerFrame.width - 70, y: 25, width: 18, height: 4)
            })
            var indexesPath = [IndexPath]()
            
            
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i , section: section)
                indexesPath.append(index)
            }
            
            self.expandedSectionHeaderNumber = section
            self.tblReport!.beginUpdates()
            self.tblReport!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            //self.TableFiles.roundedCorners(top: false)
            
            self.tblReport.tableHeaderView?.roundedCornersAll()
            self.tblReport!.endUpdates()
            
            let lastScrollOffset = tblReport.contentOffset
            tblReport.layer.removeAllAnimations()
            tblReport.setContentOffset(lastScrollOffset, animated: false)
            
            self.tblReport.reloadData()
        }


    }
    
    
}
