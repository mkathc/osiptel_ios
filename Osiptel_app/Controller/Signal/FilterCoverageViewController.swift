//
//  FilterCoverageViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 30/01/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FilterCoverageViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var OperatorListTableview: UITableView!
    @IBAction func BackActionBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    var _operadores = Operador()
    var _departamento = Departamento()
    var _provincia = Provincia()
    var _distrito = Distrito()
    var _localidad = Localidad()
    var _cobertura = CoberturaMovil()
    var _reporteCobertura = ReporteCobertura()
    var _departamentoCadena = "0"
    var _provinciaCadena = "0"
    var _distritoCadena = "0"
    var _localidaCadena = "0"
    var _operadorCadena = "15"
    var CadenaOperador : String = ""
    //Loading
    let size = CGSize(width: 30, height: 30)
    
    var operadoresFiltro = [String]()
    var operadoresFiltradas = [String]()
    var operadoresSend = [[String:String]]()
    var operadoresImagen = [String]()
    var FiltroUnico = [[String:String]]()
    var operador = [String]()
    var dvalidate = false
    var pvalidate = false
    var disvalidate = false
    var lvalidate = false
    var ovalidate = false
    
    let myColor = UIColor.red
    
    override func viewWillDisappear(_ animated: Bool) {
        _ = UINavigationBar.appearance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = item
        navigationController?.navigationBar.tintColor = .white
        
        startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor:
            UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
            , fadeInAnimation: nil)
        
        OperatorListTableview.delegate = self
        OperatorListTableview.dataSource = self
        attempFetchOperador()
        attempFetchDepartamento()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard(_:)))
        tapGesture.cancelsTouchesInView = true
        OperatorListTableview.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard(_ sender: UITapGestureRecognizer){
    //func hideKeyboard() {
     //   OperatorListTableview.endEditing(true)
    }
    
    
    func attempFetchOperador(){
        DataService.shared.requestFetchOperadores{ [weak self] (operador, error) in
            if let error = error {
                print("error operadores \(error.localizedDescription)")
                self!.stopAnimating()
                AppDelegate.showAlertView(vc: self!, titleString: global.Titulo, messageString: global.Mensaje)
            }
            guard let operador = operador else{
                return
            }
            self?.updateOperador(with: operador)
        }
    }
    
    func updateOperador(with operador: Operador) {
        _operadores = operador
        OperatorListTableview.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.OperatorListTableview.endEditing(true)
    }
    
    func dismissKeyboard() {
        OperatorListTableview.endEditing(true)
    }
    
    //Departamento
    func attempFetchDepartamento(){
        DataService.shared.requestFetchAllDepartamento{ [weak self] (departamento, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let departamento = departamento else{
                return
            }
            self?.updateDepartamento(with: departamento)
        }
    }
    
    func updateDepartamento(with departamento: Departamento) {
        _departamento = departamento
        _departamento.insert(DepartamentoElement.init(codigo: "0", departamento: "Seleccione"), at: 0)
        OperatorListTableview.reloadData()
        stopAnimating()
    }
    
    //Provincia
    func attempFetchProvincia(idDepartmento: String){
        DataService.shared.requestFetchProvincia(with: idDepartmento){ [weak self] (provincia, error) in
            if let error = error {

            }
            guard let provincia = provincia else{
                return
            }
            self?.updateProvincia(with: provincia)
        }
    }
    
    func updateProvincia(with provincia: Provincia) {
        _provincia = provincia
        _provincia.insert(ProvinciaElement.init(codigo: "0", provincia: "Seleccione" ), at: 0)
        OperatorListTableview.reloadData()
    }
    
    //Distrito
    func attempFetchDistrito(idDepartmento: String, idProvincia: String){
        DataService.shared.requestFetchDistrito(with: idDepartmento, with: idProvincia){ [weak self] (distrito, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let distrito = distrito else{
                return
            }
            
            print("\(distrito)")
            self?.updateDistrito(with: distrito)
        }
    }
    
    func updateDistrito(with distrito: Distrito) {
        _distrito = distrito
        _distrito.insert(DistritoElement.init(codigo: "0",distrito: "Seleccione"), at: 0)
        OperatorListTableview.reloadData()
    }
    
    //Localidad
    func attempFetchLocalidad(idDepartmento: String, idProvincia: String, idDistrito: String){
        DataService.shared.requestFetchLocalidad(with: idDepartmento, with: idProvincia, with: idDistrito){ [weak self] (localidad, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
            }
            guard let localidad = localidad else{
                return
            }
            self?.updateLocalidad(with: localidad)
        }
    }
    
    func updateLocalidad(with localidad: Localidad) {
        _localidad = localidad        
        _localidad.insert(LocalidadElement.init(codigo: "0", localidad: "Todos"), at: 0)
        OperatorListTableview.reloadData()
    }
    
    func attempFetchCobertura(idDepartmento: String, idProvincia: String, idDistrito: String, localidad: String, operador: String){
        
        DataService.shared.requestFetchGetCobertura(with: idDepartmento, with: idProvincia, with: idDistrito, with: localidad, with: operador){ [weak self] (cobertura, error) in
            if let error = error {
                print("error \(error.localizedDescription)")
                self!.stopAnimating()
                self!.alertViewController(titulo: "", mensaje: "No se econtraron resultados", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")

            }
            guard let cobertura = cobertura else{
                return
            }
            self?.updateCobertura(with: cobertura)
        }
    }
    
    func updateCobertura(with cobertura: CoberturaMovil) {
        
        stopAnimating()
        _cobertura = cobertura
        
        if _cobertura.count > 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ConsultMapKitViewController") as! ConsultMapKitViewController
            controller.operadoresImagen = operadoresFiltradas
            controller._coberturaMovil = _cobertura
            controller.CadenaOperador = CadenaOperador
            controller.operadoresSend = operadoresSend
            controller.modalPresentationStyle = .fullScreen
            //controller.hidesBottomBarWhenPushed = true
            present(controller, animated: true, completion: nil)
        }else{
            alertViewController(titulo: "", mensaje: "No se econtraron resultados", closeFlag: true, imageValidate: false, closeAutomatic: false, sendVC: "")
        }
    }
}


extension FilterCoverageViewController:  UITableViewDelegate, UITableViewDataSource,NVActivityIndicatorViewable, FilterTableViewCellDelegate, SenderFilterTableViewCellDelegate, SignalFilterTableViewCellDelegate{
    
    func SignalFilterTableViewCellDidSwitch(Operador: String, Imagen: String, Estado: Bool) {
        if Estado{
            operadoresFiltro.append(Operador)
            operadoresImagen.append(Imagen)
            operadoresSend.append(["id" : Operador,"imagen" : Imagen])
        }else{
            operadoresSend.removeAll{ $0["id"] == Operador}
            operadoresFiltro.removeAll{ $0 == Operador}
            operadoresImagen.removeAll{ $0 == Imagen}
        }
    }
    
    func SenderFilterTableViewCellDidLocalidad() {
        var isValidate = true
        var cont = 0
        for operador in operadoresFiltro{
            CadenaOperador += "\(operador),"
            cont = cont + 1
        }
        operadoresFiltradas = operadoresImagen
        
        UIView.animate (withDuration: 0.1, animations: {
        self.OperatorListTableview.reloadData()
        if self.OperatorListTableview.numberOfRows(inSection: 0) != 0 {
            self.OperatorListTableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        })
        
        let section = 0
        let row = 0
        let indexPath = IndexPath(row: row, section: section)
        let cell: FilterTableViewCell = OperatorListTableview.cellForRow(at: indexPath) as! FilterTableViewCell

        CadenaOperador = String(CadenaOperador.dropLast())
        if _departamentoCadena == "Seleccione" || _departamentoCadena == "0"{
            isValidate = false
            dvalidate = true
            //cell.txtDepartment.
            cell.txtDepartment.borderStyle = UITextBorderStyle.roundedRect
            cell.txtDepartment.layer.borderColor = myColor.cgColor
            cell.txtDepartment.layer.cornerRadius = 4
            cell.txtDepartment.borderColorView = UIColor.red
            cell.txtDepartment.layer.borderWidth = 1.0
        }
        if _provinciaCadena == "Seleccione" || _provinciaCadena == "0"{
            isValidate = false
            cell.txtProvince.borderStyle = UITextBorderStyle.roundedRect
            cell.txtProvince.layer.borderColor = myColor.cgColor
            cell.txtProvince.layer.cornerRadius = 4
            cell.txtProvince.borderColorView = UIColor.red
            cell.txtProvince.layer.borderWidth = 1.0
            pvalidate = true
        }
        if _distritoCadena == "Seleccione" || _distritoCadena == "0"{
            isValidate = false
            cell.txtDistrite.borderStyle = UITextBorderStyle.roundedRect
            cell.txtDistrite.layer.borderColor = myColor.cgColor
            cell.txtDistrite.layer.cornerRadius = 4
            cell.txtDistrite.borderColorView = UIColor.red
            cell.txtDistrite.layer.borderWidth = 1.0
            cell.txtLocalidad.borderStyle = UITextBorderStyle.roundedRect
            cell.txtLocalidad.layer.borderColor = myColor.cgColor
            cell.txtLocalidad.layer.cornerRadius = 4
            cell.txtLocalidad.borderColorView = UIColor.red
            cell.txtLocalidad.layer.borderWidth = 1.0
            disvalidate = true
        }
        
        if CadenaOperador == ""{
            isValidate = false
            ovalidate = true
        }
        
        if isValidate{
            startAnimating(size, message: "Cargando...", type: NVActivityIndicatorType.ballSpinFadeLoader, backgroundColor:
                UIColor(red:0.00, green:0.71, blue:0.89, alpha:1.0)
                , fadeInAnimation: nil)
            
            
            if _localidaCadena == "Todos"{
                _localidaCadena = "0"
            }

            attempFetchCobertura(idDepartmento: _departamentoCadena, idProvincia: _provinciaCadena, idDistrito: _distritoCadena, localidad: _localidaCadena, operador: CadenaOperador)
            
        }
    }
    func FilterTableViewCellDidSeleccion(Departamento: String, Provincia: String, Distrito: String, Localidad: String){
        _departamentoCadena = Departamento
        _provinciaCadena = Provincia
        _distritoCadena = Distrito
        _localidaCadena = Localidad
        OperatorListTableview.reloadData()
    }
    
    func FilterTableViewCellDidLocalidad(idDepartamento: String, idProvincia: String, idDistrito: String) {
        attempFetchLocalidad(idDepartmento: idDepartamento, idProvincia: idProvincia, idDistrito: idDistrito)
        _distritoCadena = idDistrito
    }
    
    func FilterTableViewCellDidDistrito(idDepartamento: String, idProvincia: String) {
        attempFetchDistrito(idDepartmento: idDepartamento, idProvincia: idProvincia)
        _provinciaCadena = idProvincia
    }
    
    
    func FilterTableViewCellDidProvincia(idDepartamento: String) {
        attempFetchProvincia(idDepartmento: idDepartamento)
        _departamentoCadena = idDepartamento
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        OperatorListTableview.endEditing(true)
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "filterCell", for: indexPath) as! FilterTableViewCell
            
            cell.setDepartamento(departamento: _departamento)
            cell.setProvincia(provincia: _provincia)
            cell.setDistrito(distrito: _distrito)
            cell.setLocalidad(localidad: _localidad)
            cell.delegate = self
            return cell
        }else if indexPath.row == _operadores.count + 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendFilterCell", for: indexPath) as! SendFilterTableViewCell
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOperatorMobile", for: indexPath) as! SignalFilterTableViewCell
            let operador = _operadores[indexPath.row - 1]
            cell.setOperador(operador: operador)
            operadoresFiltro.append(operador.codigoOperador)
            operadoresImagen.append(operador.nombreImagen!)
            operadoresSend.append(["id" : _operadores[indexPath.row - 1].codigoOperador, "imagen" : _operadores[indexPath.row - 1].nombreImagen!])
            
            operadoresFiltro = Array(Set(operadoresFiltro))
            operadoresImagen = Array(Set(operadoresImagen))
            
            cell.delegate = self
            return cell
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if _operadores.count == 0 {
            return 0
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if _operadores.count == 0 {
            return 0
        }else{
            return _operadores.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 300
            
        }else if indexPath.row == _operadores.count + 1{
            return 120
            
        }else {
            return 60
        }
    }
}
