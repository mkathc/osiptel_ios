//
//  ConsultMapKitViewController.swift
//  Osiptel_app
//
//  Created by Anthony Montes Larios on 3/5/19.
//  Copyright © 2019 Anthony Montes Larios. All rights reserved.
//

import UIKit
import MapKit
import NVActivityIndicatorView
import CoreLocation

class ConsultMapKitViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate,NVActivityIndicatorViewable{

    enum CardState {
        case expanded
        case collapsed
    }
    
    var cardViewController:CardViewController!
    var visualEffectView:UIVisualEffectView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    let cardHeight:CGFloat = 600
    let cardHandleAreaHeight:CGFloat = 65
    
    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted:CGFloat = 0

    
    var CadenaOperador = ""
    var _coberturaMovil = CoberturaMovil()
    var _coberturaMovilFiltrada = CoberturaMovil()
    var stateView = false
    var _coberturaElement : CoberturaMovilElement!
    @IBOutlet weak var mapkitSeñal: MKMapView!
    @IBOutlet weak var viewLocationMap: UIView!

    var _reporteCobertura = ReporteCobertura()
    var operadoresFiltro : [String] = []
    var operadoresImagen = [String]()
    var ImagenesString = [String]()
    var tecnologiaArray = [String]()
    var empresaArray : [String] = []
    var operadoresSend = [[String:String]]()
    private let greenView = UIView()

    //ViewController Ver Reportes - Consultar
    var tieneCobertura = false
    var count = 0
    
    @IBAction func btnShareAction(_ sender: Any) {
        let someText:String = ""
        let objectsToShare:URL = URL(string: UserDefaults.standard.getAppGlobalEnlaceCob())!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivityType.openInIBooks, UIActivityType.print, UIActivityType.addToReadingList,  UIActivityType.postToVimeo, UIActivityType.assignToContact]
        
        activityViewController.navigationController?.navigationBar.tintColor = UIColor.white
        activityViewController.navigationController?.navigationBar.barTintColor = UIColor.white
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @objc private final func tapOnLocation(_ tapGesture: UITapGestureRecognizer){
        centerViewOnUserLocation()
    }
    
    func centerViewOnUserLocation(){
        if let location = manager.location?.coordinate{
            let span = MKCoordinateSpan.init(latitudeDelta: 0.0075, longitudeDelta: 0.0075)
            let region = MKCoordinateRegion.init(center: location, span: span)
            mapkitSeñal.setRegion(region, animated: true)
        }
    }
    
    //ViewController Reportar
    @IBAction func btnCloseAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CustomTabBarView") as! CustomTabBarView
        controller.indexPosition = 3
        self.present(controller, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            //navBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
            //let barFrame = searchController.searchBar.frame
           // navigationController?.navigationBar.frame = CGRect(x: 0, y: 30, width: 30, height: 44)

           // let startingYPos = UIApplication.shared.statusBarFrame.size.height;
            //navBar = UINavigationBar(frame: CGRect(x: 0, y: 100, width: self.view.bounds.width, height: 44));
            
        }
    
        for empresa in (_coberturaMovil.first!.tecnologisOperador)!{
            empresaArray.append("\(empresa.codEmpresa)")
        }
                
        for send in empresaArray{
            ImagenesString.append(operadoresSend.filter{$0["id"] == send}.first!["imagen"]!)
        }
  
        addAnnotationPoint()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnLocation(_:)))
        viewLocationMap.addGestureRecognizer(tapGesture)
    }
 
    func setupCard() {
        visualEffectView = UIVisualEffectView()
        visualEffectView.frame = self.view.frame
        
        self.view.addSubview(visualEffectView)
        visualEffectView.alpha = 0
        cardViewController = CardViewController(nibName:"CardViewController", bundle:nil)
        cardViewController._coberturaElement = _coberturaElement!
        cardViewController.operadoresImagen = ImagenesString
        cardViewController.operadoresFiltro = operadoresFiltro
        //cardViewController.operadoresSend = operadoresSend
        self.addChildViewController(cardViewController)
        self.view.addSubview(cardViewController.view)

        cardViewController.view.frame = CGRect(x: 0, y: self.view.frame.height - cardHandleAreaHeight, width: self.view.bounds.width, height: cardHeight)
        
        cardViewController.view.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleCardTap(recognzier:)))
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleCardPan(recognizer:)))
        
        cardViewController.handleArea.addGestureRecognizer(tapGestureRecognizer)
        cardViewController.handleArea.addGestureRecognizer(panGestureRecognizer)
        

    }
    
    @objc
    func handleCardTap(recognzier:UITapGestureRecognizer) {
        switch recognzier.state {
        case .ended:
            animateTransitionIfNeeded(state: nextState, duration: 0.9)
        default:
            break
        }
    }
    
    @objc
    func handleCardPan (recognizer:UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            startInteractiveTransition(state: nextState, duration: 0.9)
        case .changed:
            let translation = recognizer.translation(in: self.cardViewController.handleArea)
            
            var fractionComplete = translation.y / cardHeight
            fractionComplete = cardVisible ? fractionComplete : -fractionComplete
            updateInteractiveTransition(fractionCompleted: fractionComplete)
        case .ended:
            continueInteractiveTransition()
        default:
            break
        }
        
    }
    
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.cardHeight
                    self.cardViewController.btnExpand.image = UIImage(named:"circle_down")
                    self.visualEffectView.alpha = 1.0
                case .collapsed:
                    self.cardViewController.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
                    self.cardViewController.btnExpand.image = UIImage(named:"circle_up")
                    self.visualEffectView.alpha = 0.0
                }
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
            
            
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    self.cardViewController.view.layer.cornerRadius = 12
                    self.cardViewController.handleArea.layer.cornerRadius = 12
                    self.cardViewController.btnExpand.image = UIImage(named:"circle_down")
                    
                case .collapsed:
                    self.cardViewController.view.layer.cornerRadius = 0
                    self.cardViewController.handleArea.layer.cornerRadius = 12
                    self.cardViewController.btnExpand.image = UIImage(named:"circle_up")
                }
            }
            
            cornerRadiusAnimator.startAnimation()
            runningAnimations.append(cornerRadiusAnimator)
            
            let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.visualEffectView.effect = UIBlurEffect(style: .dark)
                case .collapsed:
                    self.visualEffectView.effect = nil
                }
            }
            
            blurAnimator.startAnimation()
            runningAnimations.append(blurAnimator)
            
        }
    }
    
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        for animator in runningAnimations {
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
            print("animator.fractionComplete ")
            print(animator.fractionComplete)
            //animationProgressWhenInterrupted = visualEffectView.alpha = 0
        }
    }
    
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        for animator in runningAnimations {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
            visualEffectView.alpha = fractionCompleted + animationProgressWhenInterrupted
            if fractionCompleted + animationProgressWhenInterrupted > 1{
                visualEffectView.alpha = 1.0
                self.cardViewController.btnExpand.image = UIImage(named:"circle_down")
            }else{
                visualEffectView.alpha = 0.0
                self.cardViewController.btnExpand.image = UIImage(named:"circle_up")
            }
        }        
    }
    
    func continueInteractiveTransition (){
        for animator in runningAnimations {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
            if visualEffectView.alpha < 0.25 {
                visualEffectView.alpha = 0.0
                self.cardViewController.btnExpand.image = UIImage(named:"circle_up")
            }else{
                visualEffectView.alpha = 1.0
                self.cardViewController.btnExpand.image = UIImage(named:"circle_down")
            }
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }
        
        let identifier = "CustomAnnotation"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
            
        } else {
            annotationView!.annotation = annotation
        }
        
        if annotation.subtitle == "true"{
            annotationView!.image = UIImage(named: "pin_celeste")!
        }else{
            annotationView!.image = UIImage(named: "pin_gris")
        }
                
        if #available(iOS 12, *){
            print("iosVersion 12")
        }else{
            let tapGestureAnnotation = MyNewTapGesture(target: self, action: #selector(tapOnLocationAnnotation))
            let cpa = annotationView!.annotation
            tapGestureAnnotation.latitudeA = "\(cpa!.coordinate.latitude)"  as? String ?? ""
            tapGestureAnnotation.longitudeA = "\(cpa!.coordinate.longitude)"  as? String ?? ""
            tapGestureAnnotation.annotationViewMap = annotationView!
            annotationView?.addGestureRecognizer(tapGestureAnnotation)
        }
        
        return annotationView
    }
    
    @objc func tapOnLocationAnnotation(sender: MyNewTapGesture){
        for childView:AnyObject in sender.annotationViewMap.subviews{
            childView.removeFromSuperview();
        }
        let annotation = sender.annotationViewMap.annotation
        let latitudeA: String = sender.latitudeA
        let longitudeA:String = sender.longitudeA
        
        _coberturaElement = _coberturaMovil.first { $0.longitud == longitudeA  && $0.latitud == latitudeA}
        let region = MKCoordinateRegion(center: sender.annotationViewMap.annotation!.coordinate, span: mapkitSeñal.region.span)
        mapkitSeñal.setRegion(region, animated: true)
        self.setupCard()
    }
    
    
    func addAnnotationPoint()
    {
        for data in _coberturaMovil
        {
            let point = MKPointAnnotation()
            point.subtitle = String(data.tieneCobertura)
            let pointlatitude = Double(data.latitud)!
            let pointlongitude = Double(data.longitud)!
            var annotationView: MKAnnotationView?
            point.coordinate = CLLocationCoordinate2DMake(pointlatitude ,pointlongitude)
            self.mapkitSeñal.addAnnotation(point)
        }
        let span = MKCoordinateSpanMake(1.025, 1.025)

        let latitudInicial = Double(_coberturaMovil[0].latitud)!
        let longitudInicial = Double(_coberturaMovil[0].longitud)!
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitudInicial, longitude: longitudInicial), span: span)
        self.mapkitSeñal.setRegion(region, animated: true)
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        let annotation = view.annotation
        let latitudeA: String = "\(annotation!.coordinate.latitude)"  as? String ?? ""
        let longitudeA:String = "\(annotation!.coordinate.longitude)"  as? String ?? ""
     
        _coberturaElement = _coberturaMovil.first { $0.longitud == longitudeA  && $0.latitud == latitudeA}
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span)
        mapkitSeñal.setRegion(region, animated: true)
        self.setupCard()
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        tecnologiaArray.removeAll()
        self.visualEffectView.alpha = 0.0
    }
}

class MyNewTapGesture: UITapGestureRecognizer {
    var latitudeA = String()
    var longitudeA = String()
    var annotationViewMap = MKAnnotationView()
}


